#define MOTOR_64_MEX

#include "mex.h"
#include <motor.h>

#define  rows 1
#define  cols 1
#define  TOTAL_ELEMENTS (rows * cols)

extern int                          CONFIG_item;
extern struct  CONFIG_variable      CONFIG_cnfg[];

#include "GraphicsConfig.cpp"

typedef int (*cfg_func)(char*);

class fCont{
/**
 * Abstract Container class that allows exploiting templates (polymorphism)
 * To the max in what could be considetered as a bit of a hack
 * This allows us to instantiate an array without specigying the the type
 * <T> of the templated objects it holds  {<T1> , ...., <TN>}
**/
public:
    virtual mxArray  * wrapMap(void * vptr, int m, int n) = NULL ;
};

template< class T >
class FunctorContainer : public fCont { // Inheritance allows the array signature to be typeless
/**
 * FunctorContainer allows us to have a array of functions with different type 
 * signatures. This utility allowed for a major abstraction in the code
 * base reducing the need to write a new module  that void wraped all the 
 * different functions and then applied them using cases and redundant repitions of code
**/
public:
    T (*f)(void*); // Function pointer with template return type T
    FunctorContainer(T (*func)(void*) ){f=func;}; // Const
    
    mxArray * fCont::wrapMap (void * vptr, int m, int n){
      return mexMap( vptr, f , m, n);
    }
};

// We can index into this array in O(1) in order 
// to parse a datatype rather than have 12 cases
// or 12 if statements  
fCont * array_convertor[12] = {
  NULL, // NONETYPE_PLACEHOLDER
  new FunctorContainer<char>(CONFIG_char),
  new FunctorContainer<int>(CONFIG_int),
  new FunctorContainer<long>(CONFIG_long),
  new FunctorContainer<float>(CONFIG_float),
  new FunctorContainer<double>(CONFIG_double), 
  NULL, // Place for string
  new FunctorContainer<BOOL>(CONFIG_bool), 
  new FunctorContainer<short>(CONFIG_short),
  new FunctorContainer<USHORT>(CONFIG_ushort),
  NULL, // Matrix placeholder
  new FunctorContainer<USHORT>(CONFIG_ushort),
};


template <typename T>
mxArray  * mexMap (void * vptr, T (*f)(void*), int m, int n)
{
/**
 *  This function creates a mex matrix  out of the a data field in a 
 *  config item.  
 *
 *  @param vptr : pointer to a data field in a CONFIG_Variable struct
 *  @param f    : function that will extract/cast data from vptr
 *  @param m    : the number of rows in vptr (not used)
 *  @param n    : the number of collumns in vptr (number of items)
 *  @template T : the return type of function f
 *
 *  @returns    : a mexDoubleMatrix with the data in this field of the config
**/
mxArray  *field_value;
int index = 0;
double    *pr;

    field_value = mxCreateDoubleMatrix(m, n, mxREAL);
    pr = mxGetPr(field_value);
    for(index = 0; index < n; index++)
    {
       pr[index] =  f((T*)vptr + index);
    }
    return field_value;
}

void  replaceChar(char * ch, char x, char y)
{
/**
 *  this function replaces character x by character y in ch
 *
 *  @param ch : the initial char array to replace in
 *  @param x  : the char to replace
 *  @param y  : the target char to replace with
 *
 *  @return   : void does the replacement in place
**/
int index=0;

    while(ch[index])
    {
        if(ch[index] == x)
        {
            ch[index] = y;
        }
        index++;
    }    
}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
/**
 *  Mex Function that parses a MOTOR.lib config into matlab
 * TODO: Document type signature
**/
const size_t   ndim = 2, dims[2] = {rows, cols};
const char * f_names[CONFIG_ITEMS+1];
double    *pr;
mxArray  *field_value, *struct_array_ptr; 
int       index;
mxArray *array_ptr;
std::string cnfg_type;
void * vptr;
int type;
BOOL ok;
char* file_name;
cfg_func cnfg_func;
std::map<std::string, cfg_func> cnfg_map;
cnfg_map["GRAPHICS"] = GRAPHICS_Cnfg;
cnfg_map["LIBERTY"] = LIBERTY_ConfigLoad;


    if(nrhs != 2  && nrhs != 1)
    {
        mexErrMsgIdAndTxt("MATLAB:revord:invalidNumInputs",
                          "One or two inputs required.");
    }
    // Check if one or two arguments (and that they are strings)
    else if(nrhs > 1 && mxIsChar(prhs[1]) == 1 && mxIsChar(prhs[0]) == 1) 
    {
        file_name = mxArrayToString(prhs[1]);
    }
    else if( mxIsChar(prhs[0]) == 1 && nrhs  == 1)
    {
        file_name = STR_stringf("%s.CFG", mxArrayToString(prhs[0]));
    }
    // Throw matlab error if inputs are not a string
    else
    {
        mexErrMsgIdAndTxt("MATLAB:revord:inputNotString",
                          "Input must be a string.");
    }


    cnfg_type = (std::string)mxArrayToString(prhs[0]);
    cnfg_func = cnfg_map[cnfg_type];
    // If provided config mode not found
    if (cnfg_func == NULL)
    {
        mexErrMsgIdAndTxt("MATLAB:invalidConfigType",
                          "Invalid config type");
    }
    
    MOTOR_Init(); // Kicking of motor lib
    ok = cnfg_func(file_name);
    
    for( index=0; (index < CONFIG_item); index++ )
    {
        replaceChar(CONFIG_cnfg[index].name, ',', '_');
        f_names[index] =  CONFIG_cnfg[index].name;
    }
    
    struct_array_ptr = mxCreateStructArray(ndim, dims, 
	                                       CONFIG_item, f_names);
    
    for(index = 0; index < CONFIG_item; index++)
    {
        vptr = CONFIG_cnfg[index].vptr;
        type = CONFIG_cnfg[index].type;
        if ( CONFIG_type(type,vptr) == CONFIG_TYPE_STRING)
        {   
           field_value = mxCreateString(CONFIG_string(vptr));
        }
        else if ( CONFIG_type(type,vptr) != CONFIG_TYPE_NONE && array_convertor[type] != NULL)
        {
            field_value = array_convertor[type]->wrapMap(vptr, 1, CONFIG_cnfg[index].indx[2]);
        }
        mxSetField(struct_array_ptr, 0, CONFIG_cnfg[index].name, field_value); 
    }
    
    plhs[0] = (mxArray *) struct_array_ptr;
	return;
}