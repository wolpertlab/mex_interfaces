/******************************************************************************/

int     HardwareState=-1;
double  HardwareStateTime=0.0;

#define HARDWARE_DATA_FIELDS  256
#define HARDWARE_DATA_FRAMES 30000

int     HardwareDataFlag=FALSE;
int     HardwareDataFieldCount;
int     HardwareDataCount=0;
TIMER   HardwareDataTimer("HardwareDataTimer");

double  HardwareDataTime[HARDWARE_DATA_FRAMES];
int     HardwareDataState[HARDWARE_DATA_FRAMES];
double  HardwareDataStateTime[HARDWARE_DATA_FRAMES];

double  HardwareDataRobotActive[ROBOTS][HARDWARE_DATA_FRAMES];
double  HardwareDataRobotRampValue[ROBOTS][HARDWARE_DATA_FRAMES];
double  HardwareDataRobotFieldRampValue[ROBOTS][HARDWARE_DATA_FRAMES];
double  HardwareDataRobotFieldType[ROBOTS][HARDWARE_DATA_FRAMES];
double  HardwareDataRobotPosition[ROBOTS][HARDWARE_DATA_FRAMES][3];
double  HardwareDataRobotVelocity[ROBOTS][HARDWARE_DATA_FRAMES][3];
double  HardwareDataRobotForces[ROBOTS][HARDWARE_DATA_FRAMES][3];

double  HardwareDataEyeLinkFrame[HARDWARE_DATA_FRAMES];
double  HardwareDataEyeLinkTimeStamp[HARDWARE_DATA_FRAMES];
double  HardwareDataEyeLinkEyeXY[HARDWARE_DATA_FRAMES][2];
double  HardwareDataEyeLinkPupilSize[HARDWARE_DATA_FRAMES];

double  HardwareDataLibertyFrame[HARDWARE_DATA_FRAMES];
double  HardwareDataLibertyTimeStamp[HARDWARE_DATA_FRAMES];
double  HardwareDataLibertyPosition[LIBERTY_SENSOR_MAX][HARDWARE_DATA_FRAMES][3];
double  HardwareDataLibertyOrientation[LIBERTY_SENSOR_MAX][HARDWARE_DATA_FRAMES][3];
double  HardwareDataLibertyDistortion[LIBERTY_SENSOR_MAX][HARDWARE_DATA_FRAMES];

// Hardware data field types.
#define HARDWARE_DATA_GENERAL                   100
#define HARDWARE_DATA_GENERAL_TIME              101
#define HARDWARE_DATA_GENERAL_STATE             102
#define HARDWARE_DATA_GENERAL_STATETIME         103

#define HARDWARE_DATA_ROBOT                     200
#define HARDWARE_DATA_ROBOT_ACTIVE              201
#define HARDWARE_DATA_ROBOT_ROBOT_RAMP          202
#define HARDWARE_DATA_ROBOT_FIELD_RAMP          203
#define HARDWARE_DATA_ROBOT_FIELD_TYPE          204
#define HARDWARE_DATA_ROBOT_POSITION            205
#define HARDWARE_DATA_ROBOT_VELOCITY            206
#define HARDWARE_DATA_ROBOT_FORCES              207

#define HARDWARE_DATA_EYELINK                   300
#define HARDWARE_DATA_EYELINK_FRAME             301
#define HARDWARE_DATA_EYELINK_TIMESTAMP         302
#define HARDWARE_DATA_EYELINK_POSITION          303
#define HARDWARE_DATA_EYELINK_PULILSIZE         304

#define HARDWARE_DATA_LIBERTY                   400
#define HARDWARE_DATA_LIBERTY_FRAME             401
#define HARDWARE_DATA_LIBERTY_TIMESTAMP         402
#define HARDWARE_DATA_LIBERTY_POSITION          403
#define HARDWARE_DATA_LIBERTY_ORIENTATION       404
#define HARDWARE_DATA_LIBERTY_DISTORTION        405

/******************************************************************************/

CRITICAL_SECTION HardwareDataCriticalSection;

/******************************************************************************/

BOOL HardwareDataStart( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;

    if( HardwareDataFlag )
    {
        mexPrintf("mexRobotFunc: Data already started.\n");
        ok = FALSE;
    }
    else
    if( LOOPTASK_running(HardwareDataLoop) )
    {
        mexPrintf("mexRobotFunc: Hardware data loop is already running.\n");
        ok = FALSE;
    }        
    else
    if( !LOOPTASK_start(HardwareDataLoop) )
    {
        mexPrintf("mexRobotFunc: Cannot start hardware data loop.\n");
        ok = FALSE;
    }

    if( !ok )
    {
        LOOPTASK_stop(HardwareDataLoop);
        return(FALSE);
    }

    EnterCriticalSection(&HardwareDataCriticalSection);
    
    HardwareDataFlag = TRUE;
    HardwareDataCount = 0;
    HardwareDataTimer.Reset();
    
    LeaveCriticalSection(&HardwareDataCriticalSection);
    
    return(ok);
}

/******************************************************************************/

BOOL HardwareDataStop( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;

    if( !HardwareDataFlag )
    {
        mexPrintf("mexRobotFunc: Data not started.\n");
        ok = FALSE;
    }
    else
    if( !LOOPTASK_running(HardwareDataLoop) )
    {
        mexPrintf("mexRobotFunc: Hardware data loop is not running.\n");
        ok = FALSE;
    }        
        
    if( !ok )
    {
        return(FALSE);
    }

    // Stop data collection, but collect minimum number of frames.
    do
    {
        EnterCriticalSection(&HardwareDataCriticalSection);
    
        // Collect at least N frames before stopping.
        if( HardwareDataCount >= 2 )
        {
            HardwareDataFlag = FALSE;
        }
    
        LeaveCriticalSection(&HardwareDataCriticalSection);
        
        // Still waiting for N frames.
        if( HardwareDataFlag )
        {
            Sleep(1);
        }
    }
    while( HardwareDataFlag );

    LOOPTASK_stop(HardwareDataLoop);
    
    mexPrintf("mexRobotFunc: %d frames of data collected.\n",HardwareDataCount);
    
    return(ok);
}

/******************************************************************************/

BOOL HardwareDataGet( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int robot,sensor;
int i,j,k;
int fcount,flist[HARDWARE_DATA_FIELDS];
double *dptr;

    if( HardwareDataFlag )
    {
        mexPrintf("mexRobotFunc: Data still running.\n");
        plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL); 
        ok = FALSE;
        return(ok);
    }

    if( HardwareDataCount == 0 )
    {
        mexPrintf("mexRobotFunc: No data to return.\n");
        plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL); 
        ok = FALSE;
        return(ok);
    }

    EnterCriticalSection(&HardwareDataCriticalSection);

    // Create list of field types...
    fcount = 0;
    
    // General fields.
    flist[fcount++] = HARDWARE_DATA_GENERAL_TIME;
    flist[fcount++] = HARDWARE_DATA_GENERAL_STATE;
    flist[fcount++] = HARDWARE_DATA_GENERAL_STATETIME;
    
    // Device-specific fields.
    if( RobotStarted() )
    {
        for( robot=0; (robot < RobotCount); robot++ )
        {
            flist[fcount++] = HARDWARE_DATA_ROBOT_ACTIVE;
            flist[fcount++] = HARDWARE_DATA_ROBOT_ROBOT_RAMP;
            flist[fcount++] = HARDWARE_DATA_ROBOT_FIELD_RAMP;
            flist[fcount++] = HARDWARE_DATA_ROBOT_FIELD_TYPE;
            
            for( i=0; (i < 3); i++ )
            {
                flist[fcount++] = HARDWARE_DATA_ROBOT_POSITION;
            }
            
            for( i=0; (i < 3); i++ )
            {
                flist[fcount++] = HARDWARE_DATA_ROBOT_VELOCITY;
            }
            
            for( i=0; (i < 3); i++ )
            {
                flist[fcount++] = HARDWARE_DATA_ROBOT_FORCES;
            }
        }
    }
    
    if( EyeLinkStarted() )
    {
        flist[fcount++] = HARDWARE_DATA_EYELINK_FRAME;
        flist[fcount++] = HARDWARE_DATA_EYELINK_TIMESTAMP;
        flist[fcount++] = HARDWARE_DATA_EYELINK_POSITION; // x-axis
        flist[fcount++] = HARDWARE_DATA_EYELINK_POSITION; // y-axis
        flist[fcount++] = HARDWARE_DATA_EYELINK_PULILSIZE;
    }
    
    if( LibertyStarted() )
    {
        flist[fcount++] = HARDWARE_DATA_LIBERTY_FRAME;
        flist[fcount++] = HARDWARE_DATA_LIBERTY_TIMESTAMP;
        
        for( sensor=0; (sensor < LibertySensorCount); sensor++ )
        {
            flist[fcount++] = HARDWARE_DATA_LIBERTY_POSITION; // x-axis
            flist[fcount++] = HARDWARE_DATA_LIBERTY_POSITION; // y-axis
            flist[fcount++] = HARDWARE_DATA_LIBERTY_POSITION; // z-axis
            
            flist[fcount++] = HARDWARE_DATA_LIBERTY_ORIENTATION; // x-axis
            flist[fcount++] = HARDWARE_DATA_LIBERTY_ORIENTATION; // y-axis
            flist[fcount++] = HARDWARE_DATA_LIBERTY_ORIENTATION; // z-axis
            
            flist[fcount++] = HARDWARE_DATA_LIBERTY_DISTORTION;
        }
    }
    
    HardwareDataFieldCount = fcount;
    
    mexPrintf("HardwareDataGet: fields=%d, frames=%d.\n",HardwareDataFieldCount,HardwareDataCount);
    
    // Create the matrix which will contain the frame data.
    plhs[1] = mxCreateDoubleMatrix(HardwareDataFieldCount,HardwareDataCount+1,mxREAL); 
    dptr = mxGetPr(plhs[1]);
    
    // First put the field types in the first frame of data.
    for( j=0,i=0; (i < HardwareDataFieldCount); i++ )
    {
        dptr[j++] = (double)flist[i];
    }
    
    // Now do the frames of data.
    for( k=0; (k < HardwareDataCount); k++ )
    {
        // General frame data here.
        dptr[j++] = HardwareDataTime[k];
        dptr[j++] = HardwareDataState[k];
        dptr[j++] = HardwareDataStateTime[k];
        
        // Device-specific frame data here.
        if( RobotStarted() )
        {
            for( robot=0; (robot < RobotCount); robot++ )
            {
                dptr[j++] = HardwareDataRobotActive[robot][k];
                dptr[j++] = HardwareDataRobotRampValue[robot][k];
                dptr[j++] = HardwareDataRobotFieldRampValue[robot][k];
                dptr[j++] = HardwareDataRobotFieldType[robot][k];

                for( i=0; (i < 3); i++ )
                {
                    dptr[j++] = HardwareDataRobotPosition[robot][k][i];
                }

                for( i=0; (i < 3); i++ )
                {
                    dptr[j++] = HardwareDataRobotVelocity[robot][k][i];
                }

                for( i=0; (i < 3); i++ )
                {
                    dptr[j++] = HardwareDataRobotForces[robot][k][i];
                }
            }
        }
        
        if( EyeLinkStarted() )
        {
            dptr[j++] = HardwareDataEyeLinkFrame[k];
            dptr[j++] = HardwareDataEyeLinkTimeStamp[k];
            dptr[j++] = HardwareDataEyeLinkEyeXY[k][0];
            dptr[j++] = HardwareDataEyeLinkEyeXY[k][1];
            dptr[j++] = HardwareDataEyeLinkPupilSize[k];
        }
        
        if( LibertyStarted() )
        {
            dptr[j++] = HardwareDataLibertyFrame[k];
            dptr[j++] = HardwareDataLibertyTimeStamp[k];
        
            for( sensor=0; (sensor < LibertySensorCount); sensor++ )
            {
                dptr[j++] = HardwareDataLibertyPosition[sensor][k][0]; // x-axis
                dptr[j++] = HardwareDataLibertyPosition[sensor][k][1]; // y-axis
                dptr[j++] = HardwareDataLibertyPosition[sensor][k][2]; // z-axis

                dptr[j++] = HardwareDataLibertyOrientation[sensor][k][0]; // x-axis
                dptr[j++] = HardwareDataLibertyOrientation[sensor][k][1]; // y-axis
                dptr[j++] = HardwareDataLibertyOrientation[sensor][k][2]; // z-axis

                dptr[j++] = HardwareDataLibertyDistortion[sensor][k];
            }
        }
    }    
    
    LeaveCriticalSection(&HardwareDataCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL HardwareDataStateSet( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;

    EnterCriticalSection(&HardwareDataCriticalSection);
    
    // Get the function code from the input parameter.
    HardwareState = (int)(*mxGetPr(prhs[1]));
    HardwareStateTime = (double)(*mxGetPr(prhs[2]));
    
    LeaveCriticalSection(&HardwareDataCriticalSection);
    
    return(ok);
}

/******************************************************************************/

void HardwareDataRobot( void )
{
int robot,i;

    if( !RobotStarted() )
    {
        return;
    }
        
    EnterCriticalSection(&RobotLoopCriticalSection);
    
    for( robot=0; (robot < RobotCount); robot++ )
    {
        // Why are these not using the "Latest" variables?
        HardwareDataRobotActive[robot][HardwareDataCount] = RobotActive[robot];
        HardwareDataRobotRampValue[robot][HardwareDataCount] = RobotRampValue[robot];
        HardwareDataRobotFieldRampValue[robot][HardwareDataCount] = RobotFieldRampValue[robot];
        HardwareDataRobotFieldType[robot][HardwareDataCount] = RobotFieldType[robot];

        // Why are these using the "Latest" variables?
        for( i=0; (i < 3); i++ )
        {
            HardwareDataRobotPosition[robot][HardwareDataCount][i] = RobotLatestPosition[robot][i];
            HardwareDataRobotVelocity[robot][HardwareDataCount][i] = RobotLatestVelocity[robot][i];
            HardwareDataRobotForces[robot][HardwareDataCount][i] = RobotLatestForces[robot][i];
        }
    }

    LeaveCriticalSection(&RobotLoopCriticalSection);
}

/******************************************************************************/

void HardwareDataEyeLink( void )
{
    if( !EyeLinkStarted() )
    {
        return;
    }
    
    EnterCriticalSection(&EyeLinkLoopCriticalSection);
    
	HardwareDataEyeLinkFrame[HardwareDataCount] = (double)EyeLinkLatestFrame;
	HardwareDataEyeLinkTimeStamp[HardwareDataCount] = EyeLinkLatestTimeStamp;
	HardwareDataEyeLinkEyeXY[HardwareDataCount][0] = EyeLinkLatestEyeXY[0];
	HardwareDataEyeLinkEyeXY[HardwareDataCount][1] = EyeLinkLatestEyeXY[1];
	HardwareDataEyeLinkPupilSize[HardwareDataCount] = EyeLinkLatestPupilSize;

    LeaveCriticalSection(&EyeLinkLoopCriticalSection);
}

/******************************************************************************/

void HardwareDataLiberty( void )
{
int sensor;

    if( !LibertyStarted() )
    {
        return;
    }
    
    EnterCriticalSection(&LibertyLoopCriticalSection);
    
	HardwareDataLibertyFrame[HardwareDataCount] = (double)LibertyLatestFrame;
	HardwareDataLibertyTimeStamp[HardwareDataCount] = LibertyLatestTimeStamp;

    for( sensor=0; (sensor < LibertySensorCount); sensor++ )
    {
        HardwareDataLibertyPosition[sensor][HardwareDataCount][0] = LibertyLatestPosition[sensor](1,1);
        HardwareDataLibertyPosition[sensor][HardwareDataCount][1] = LibertyLatestPosition[sensor](2,1);
        HardwareDataLibertyPosition[sensor][HardwareDataCount][2] = LibertyLatestPosition[sensor](3,1);
        
        HardwareDataLibertyOrientation[sensor][HardwareDataCount][0] = LibertyLatestOrientation[sensor](1,1);
        HardwareDataLibertyOrientation[sensor][HardwareDataCount][1] = LibertyLatestOrientation[sensor](2,1);
        HardwareDataLibertyOrientation[sensor][HardwareDataCount][2] = LibertyLatestOrientation[sensor](3,1);
        
        HardwareDataLibertyDistortion[sensor][HardwareDataCount] = LibertyLatestDistortion[sensor];
    }
    
    LeaveCriticalSection(&LibertyLoopCriticalSection);
}

/******************************************************************************/

void HardwareDataLoop( void )
{
    if( !HardwareDataFlag || (HardwareDataCount == HARDWARE_DATA_FRAMES) )
    {
        return;
    }
    
    EnterCriticalSection(&HardwareDataCriticalSection);
    
    // General frame data here.
    HardwareDataTime[HardwareDataCount] = HardwareDataTimer.ElapsedSeconds();
    HardwareDataState[HardwareDataCount] = HardwareState;
    HardwareDataStateTime[HardwareDataCount] = HardwareStateTime;

    // Device-specific frame data here.
    HardwareDataRobot();   
    HardwareDataEyeLink();
    HardwareDataLiberty();
    
    // Increment frame count.
    HardwareDataCount++;

    LeaveCriticalSection(&HardwareDataCriticalSection);
}

/******************************************************************************/


