/******************************************************************************/
/*                                                                            */
/* MODULE  : mexHardwareFunc.cpp                                              */
/*                                                                            */
/* PURPOSE : Matlab mex function for hardware interfacing with MOTOR.LIB      */
/*                                                                            */
/* DATE    : 25/Jan/2017                                                      */
/*                                                                            */
/* CHANGES                                                                    */
/*                                                                            */
/* V0.0  JNI 25/Jan/2017 - Initial development.                               */
/*                                                                            */
/* V1.0  JNI 09/Feb/2017 - Ongoing development and re-organization of code.   */
/*                                                                            */
/******************************************************************************/

#define MOTOR_64_MEX

#include <motor.h>
#include "mex.h"

/******************************************************************************/

#define HARDWARE_INVALID                       -1
#define HARDWARE_ROBOT_START                    0
#define HARDWARE_ROBOT_STOP                     1
#define HARDWARE_ROBOT_LATEST                   2
#define HARDWARE_DATA_START                     3
#define HARDWARE_DATA_STOP                      4
#define HARDWARE_DATA_GET                       5
#define HARDWARE_DATA_STATE_SET                 6
#define HARDWARE_ROBOT_RAMP_UP                  7
#define HARDWARE_ROBOT_RAMP_DOWN                8
#define HARDWARE_ROBOT_FIELD_NONE               9
#define HARDWARE_ROBOT_FIELD_VISCOUS           10
#define HARDWARE_ROBOT_FIELD_SPRING            11
#define HARDWARE_ROBOT_FIELD_CHANNEL           12
#define HARDWARE_ROBOT_FIELD_PMOVE             13
#define HARDWARE_ROBOT_FIELD_BIMANUAL_SPRING   14
#define HARDWARE_EYELINK_START                 15
#define HARDWARE_EYELINK_STOP                  16
#define HARDWARE_EYELINK_LATEST                17
#define HARDWARE_EYELINK_SET_CALIBRATION       18
#define HARDWARE_LIBERTY_START                 19
#define HARDWARE_LIBERTY_STOP                  20
#define HARDWARE_LIBERTY_LATEST                21
#define HARDWARE_MAX                           22

//  Output arg's              0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1
int HardwareFuncMinNLHS[] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
int HardwareFuncMaxNLHS[] = { 1,1,5,1,1,2,1,1,1,1,1,1,1,1,1,1,1,5,1,2,1,6 };

//  Input arg's               0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1
int HardwareFuncMinNRHS[] = { 2,1,1,1,1,1,3,1,1,1,2,4,4,4,4,1,1,1,1,2,1,1 };
int HardwareFuncMaxNRHS[] = { 3,1,1,1,1,1,3,2,2,2,3,5,5,5,4,1,1,1,2,2,1,1 };

// Robot function codes...
int HardwareRobotFuncList[] =
{
    HARDWARE_ROBOT_START,
    HARDWARE_ROBOT_STOP,
    HARDWARE_ROBOT_LATEST,
    HARDWARE_ROBOT_RAMP_UP,
    HARDWARE_ROBOT_RAMP_DOWN,
    HARDWARE_ROBOT_FIELD_NONE,
    HARDWARE_ROBOT_FIELD_VISCOUS,
    HARDWARE_ROBOT_FIELD_SPRING,
    HARDWARE_ROBOT_FIELD_CHANNEL,
    HARDWARE_ROBOT_FIELD_PMOVE,
    HARDWARE_ROBOT_FIELD_BIMANUAL_SPRING,
    HARDWARE_INVALID
};

// Data function codes...
int HardwareDataFuncList[] =
{
	HARDWARE_DATA_START,
	HARDWARE_DATA_STOP,
	HARDWARE_DATA_GET,
    HARDWARE_DATA_STATE_SET,
    HARDWARE_INVALID
};

// EyeLink data codes...
int HardwareEyeLinkFuncList[] =
{
	HARDWARE_EYELINK_START,
	HARDWARE_EYELINK_STOP,
	HARDWARE_EYELINK_LATEST,
	HARDWARE_EYELINK_SET_CALIBRATION,
    HARDWARE_INVALID
};

// Liberty data codes...
int HardwareLibertyFuncList[] =
{
	HARDWARE_LIBERTY_START,
	HARDWARE_LIBERTY_STOP,
	HARDWARE_LIBERTY_LATEST,
    HARDWARE_INVALID
};

// Hardware start function codes...
int HardwareStartFuncList[] =
{
    HARDWARE_ROBOT_START,
	HARDWARE_EYELINK_START,
	HARDWARE_LIBERTY_START,
    HARDWARE_INVALID
};

// Hardware stop function codes...
int HardwareStopFuncList[] =
{
    HARDWARE_ROBOT_STOP,
	HARDWARE_EYELINK_STOP,
	HARDWARE_LIBERTY_STOP,
    HARDWARE_INVALID
};

BOOL HardwareRobotFuncFlag[HARDWARE_MAX];
BOOL HardwareDataFuncFlag[HARDWARE_MAX];
BOOL HardwareEyeLinkFuncFlag[HARDWARE_MAX];
BOOL HardwareLibertyFuncFlag[HARDWARE_MAX];
BOOL HardwareStartFuncFlag[HARDWARE_MAX];
BOOL HardwareStopFuncFlag[HARDWARE_MAX];

/******************************************************************************/

BOOL HardwareInitFlag=FALSE;

CRITICAL_SECTION HardwareWatchDogCriticalSection;

TIMER   HardwareWatchDogTimer("HardwareWatchDogTimer");
double  HardwareWatchDogTimeOut=1.0;
BOOL    HardwareWatchDogTimedOut=FALSE;
BOOL    HardwareWatchDogTimeOutFlag=FALSE;
int     HardwareWatchDogTimeOutCount=0;
HANDLE  HardwareWatchDogHandle=NULL;
BOOL    HardwareWatchDogExit=FALSE;
BOOL    HardwareWatchDogRunning=FALSE;
DWORD   WINAPI HardwareWatchDogFunction( LPVOID parameters );

TIMER_Frequency HardwareWatchDogLoopFrequency("HardwareWatchDogLoop");
TIMER_Interval  HardwareWatchDogLoopLatency("HardwareWatchDogLoop");

/******************************************************************************/

void HardwareDataLoop( void );
void HardwareThreadStop( BOOL *RunningFlag, BOOL *ExitFlag, char *Description );

/******************************************************************************/

#include "MexVarConvert.cpp"
#include "MexHardwareRobot.cpp"
// #include "eye_wrap.cpp"
#include "MexHardwareEyeLink.cpp"
#include "MexHardwareLiberty.cpp"
#include "MexHardwareData.cpp"

/******************************************************************************/

// List of functions for hardware function codes...
BOOL (*HardwareFunc[HARDWARE_MAX])( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) =
{
    RobotStart,
    RobotStopCritical,
    RobotGetLatest,
    HardwareDataStart,
    HardwareDataStop,
    HardwareDataGet,
    HardwareDataStateSet,
    RobotFieldRampUp,
    RobotFieldRampDown,
    RobotFieldNone,
    RobotFieldViscous,
    RobotFieldSpring,
    RobotFieldChannel,
    RobotFieldPMove,
    RobotFieldBimanualSpring,
    EyeLinkStart,
    EyeLinkStopCritical,
    EyeLinkGetLatest,
    EyeLinkSetCalibration,
    LibertyStart,
    LibertyStopCritical,
    LibertyGetLatest
};

// List of hardware stop functions...
BOOL (*HardwareStopFunc[])( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) =
{
    RobotStopCritical,
    EyeLinkStopCritical,
    LibertyStopCritical,
    NULL
};

// List of hardware started functions...
BOOL (*HardwareStartedFunc[])( void ) =
{
    RobotStarted,
    EyeLinkStarted,
    LibertyStarted,
    NULL
};

/******************************************************************************/

void HardwareInit( void )
{
int func,robot,i;

    if( HardwareInitFlag )
    {
        return;
    }

    MOTOR_Init();

    InitializeCriticalSection(&HardwareDataCriticalSection);
    InitializeCriticalSection(&RobotLoopCriticalSection);
    InitializeCriticalSection(&HardwareWatchDogCriticalSection);
    InitializeCriticalSection(&EyeLinkLoopCriticalSection);
    InitializeCriticalSection(&LibertyLoopCriticalSection);

    // Initialize device-specific function flags.
    for( func=0; (func < HARDWARE_MAX); func++ )
    {
        HardwareRobotFuncFlag[func] = FALSE;
        HardwareDataFuncFlag[func] = FALSE;
        HardwareEyeLinkFuncFlag[func] = FALSE;
        HardwareLibertyFuncFlag[func] = FALSE;
        HardwareStartFuncFlag[func] = FALSE;
        HardwareStopFuncFlag[func] = FALSE;
    }

    // Set Robot-specific function flags.
    for( i=0; (HardwareRobotFuncList[i] != HARDWARE_INVALID); i++ )
    {
        func = HardwareRobotFuncList[i];
        HardwareRobotFuncFlag[func] = TRUE;
    }

    // Set Data-specific function flags.
    for( i=0; (HardwareDataFuncList[i] != HARDWARE_INVALID); i++ )
    {
        func = HardwareDataFuncList[i];
        HardwareDataFuncFlag[func] = TRUE;
    }

    // Set EyeLink-specific function flags.
    for( i=0; (HardwareEyeLinkFuncList[i] != HARDWARE_INVALID); i++ )
    {
        func = HardwareEyeLinkFuncList[i];
        HardwareEyeLinkFuncFlag[func] = TRUE;
    }

    // Set Liberty-specific function flags.
    for( i=0; (HardwareLibertyFuncList[i] != HARDWARE_INVALID); i++ )
    {
        func = HardwareLibertyFuncList[i];
        HardwareLibertyFuncFlag[func] = TRUE;
    }

    // Set hardware start function flags.
    for( i=0; (HardwareStartFuncList[i] != HARDWARE_INVALID); i++ )
    {
        func = HardwareStartFuncList[i];
        HardwareStartFuncFlag[func] = TRUE;
    }

    // Set hardware stop function flags.
    for( i=0; (HardwareStopFuncList[i] != HARDWARE_INVALID); i++ )
    {
        func = HardwareStopFuncList[i];
        HardwareStopFuncFlag[func] = TRUE;
    }

    // Initialize Robot variables.
    for( robot=0; (robot < ROBOTS); robot++ )
    {
        RobotPosition[robot].dim(3,1);
        RobotVelocity[robot].dim(3,1);
        RobotForces[robot].dim(3,1);

        RobotControlPosition[robot].dim(3,1);
        RobotControlVelocity[robot].dim(3,1);
        RobotControlForces[robot].dim(3,1);
        RobotControlForcesLast[robot].dim(3,1);
        RobotControlForcesStep[robot].dim(3,1);

        RobotActive[robot] = FALSE;
        RobotRampValue[robot] = 0.0;
        RobotFieldRampValue[robot] = 0.0;
        RobotPMoveFinished[robot] = FALSE;

        RobotFieldType[robot] = ROBOT_FIELD_NONE;
        RobotFieldForces[robot].dim(3,1);

        RobotViscousMatrix[robot].dim(3,3);

        RobotSpringHome[robot].dim(3,1);
        RobotSpringConstant[robot] = 0.0;
        RobotDampingConstant[robot] = 0.0;

        RobotChannelStart[robot].dim(3,1);
        RobotChannelTarget[robot].dim(3,1);
        RobotChannelAngle[robot] = 0.0;

        RobotFieldRampTime[robot] = 0.1;

        RobotPMoveMoveTime[robot] = 0.5;
        RobotPMoveHoldTime[robot] = 0.0;
        RobotPMoveTarget[robot].dim(3,1);
        RobotPMoveState[robot] = 0;
        RobotPMoveStateTime[robot] = 0.0;
        RobotPMoveStateRampValue[robot] = 0.0;
        RobotPMoveStatePosition[robot].dim(3,1);

	    RobotLatestActive[robot] = FALSE;
        RobotLatestRampValue[robot] = 0.0;
        RobotLatestFieldRampValue[robot] = 0.0;
        RobotLatestPMoveFinished[robot] = FALSE;

        for( i=0; (i < 3); i++ )
        {
            RobotLatestPosition[robot][i] = 0.0;
            RobotLatestVelocity[robot][i] = 0.0;
            RobotLatestForces[robot][i] = 0.0;
        }
    }

    // Initialize EyeLink variables.
    for( i=0; (i < 2); i++ )
    {
        EyeLinkLatestEyeXY[i] = 0.0;
    }

    // Set EyeLink default calibration matrix.
    EyeLinkCalibrationMatrix.dim(2,3);
    EyeLinkCalibrationMatrix.zeros();
    EyeLinkCalibrationMatrix(1,1) = 1.0;
    EyeLinkCalibrationMatrix(2,2) = 1.0;

    // Initialize Liberty variables.
    for( i=0; (i < LIBERTY_SENSOR_MAX); i++ )
    {
        LibertyLatestPosition[i].dim(3,1),
        LibertyLatestOrientation[i].dim(3,1);
        LibertyLatestDistortion[i] = 0;
    }

    HardwareInitFlag = TRUE;
}

/******************************************************************************/

BOOL HardwareRobotFunc( int func )
{
BOOL flag;

    flag = HardwareRobotFuncFlag[func];

    return(flag);
}

/******************************************************************************/

BOOL HardwareDataFunc( int func )
{
BOOL flag;

    flag = HardwareDataFuncFlag[func];

    return(flag);
}

/******************************************************************************/

BOOL HardwareEyeLinkFunc( int func )
{
BOOL flag;

    flag = HardwareEyeLinkFuncFlag[func];

    return(flag);
}

/******************************************************************************/

void HardwareTimerResults( void )
{
    // Print results of timers...
    HardwareWatchDogLoopFrequency.Results(mexPrintf);
    HardwareWatchDogLoopLatency.Results(mexPrintf);

    // Reset timers...
    HardwareWatchDogLoopFrequency.Reset();
    HardwareWatchDogLoopLatency.Reset();
}

/******************************************************************************/

BOOL HardwareStop( void )
{
BOOL ok;
int i;

    // Loop over and run all hardware stop functions...
    for( ok=TRUE,i=0; (HardwareStopFunc[i] != NULL); i++ )
    {
        if( !(*HardwareStopFunc[i])(0,NULL,0,NULL) )
        {
            ok = FALSE;
        }
    }

    return(ok);
}

/******************************************************************************/

BOOL HardwareStarted( void )
{
BOOL flag;
int i;

    // Loop until we find some hardware that is started...
    for( flag=FALSE,i=0; ((HardwareStartedFunc[i] != NULL) && !flag); i++ )
    {
        if( (*HardwareStartedFunc[i])() )
        {
            flag = TRUE;
        }
    }

    return(flag);
}

/******************************************************************************/

void HardwareThreadStop( BOOL *RunningFlag, BOOL *ExitFlag, char *Description )
{
TIMER WaitTimer;
double WaitTimeOut=1.0,WaitTime=0.0;

    if( (*RunningFlag) )
    {
        mexPrintf("mexRobotFunc: Waiting for %s to exit...\n",Description);

        (*ExitFlag) = TRUE;
        WaitTimer.Reset();

        while( (*RunningFlag) && !WaitTimer.ExpiredSeconds(WaitTimeOut) )
        {
            WaitTime = WaitTimer.ElapsedSeconds();
        }

        mexPrintf("mexRobotFunc: %s stop %s (WaitTime=%.3f).\n",Description,STR_OkFailed(!(*RunningFlag)),WaitTime);
    }
}

/******************************************************************************/

BOOL HardwareWatchDogCheck( int func )
{
BOOL flag=TRUE;

    // Do Watch-Dog for hardware functions except robot stop.
    if( HardwareStopFuncFlag[func] )
    {
        return(TRUE);
    }

    EnterCriticalSection(&HardwareWatchDogCriticalSection);

    HardwareWatchDogTimer.Reset();
    HardwareWatchDogTimeOutFlag = FALSE;

    flag = !HardwareWatchDogTimedOut;

    LeaveCriticalSection(&HardwareWatchDogCriticalSection);

    return(flag);
}

/******************************************************************************/

BOOL HardwareWatchDogStart( void )
{
BOOL ok=TRUE;

    // If the Watch-Dog already running, do nothing.
    if( HardwareWatchDogRunning )
    {
        return(TRUE);
    }

    // Reset Watch-Dog variables.
    HardwareWatchDogExit = FALSE;
    HardwareWatchDogTimedOut = FALSE;
    HardwareWatchDogTimeOutFlag = FALSE;
    HardwareWatchDogTimeOutCount = 0;
    HardwareWatchDogTimer.Reset();

    if( (HardwareWatchDogHandle=CreateThread(NULL,0,HardwareWatchDogFunction,(void *)&HardwareWatchDogExit,0,NULL)) == NULL )
    {
        mexPrintf("mexRobotFunc: Cannot start HardwareWatchDog thread.\n");
        ok = FALSE;
    }
    else
    {
        HardwareWatchDogRunning = TRUE;
    }

    return(ok);
}

/******************************************************************************/

BOOL HardwareWatchDogStop( void )
{
BOOL ok=TRUE;

    // If there's any hardware started, don't stop the Watch-Dog...
    if( HardwareStarted() )
    {
        return(TRUE);
    }

    // Stop Watch-Dog timer thread if it's running.
    HardwareThreadStop(&HardwareWatchDogRunning,&HardwareWatchDogExit,"Hardware Watch-Dog");
    
    if( HardwareWatchDogTimeOutCount > 0 )
    {
        mexPrintf("mexRobotFunc: WatchDog timeout count=%d.\n",HardwareWatchDogTimeOutCount);
    }

    // Reset Watch-Dog variables.
    HardwareWatchDogExit = FALSE;
    HardwareWatchDogTimedOut = FALSE;
    HardwareWatchDogTimeOutFlag = FALSE;
    HardwareWatchDogTimeOutCount = 0;
    HardwareWatchDogTimer.Reset();

    HardwareTimerResults();

    return(ok);
}

/******************************************************************************/

DWORD WINAPI HardwareWatchDogFunction( LPVOID parameters )
{
BOOL Exit,ExitFlag,ok;

    // Set running flag to TRUE.
    HardwareWatchDogRunning = TRUE;

    Exit = FALSE;

    while( !Exit )
    {
        HardwareWatchDogLoopFrequency.Loop();

        Sleep(10);

        HardwareWatchDogLoopLatency.Before();

        // Try enter critical section; skip this iteration if we can't.
        if( TryEnterCriticalSection(&HardwareWatchDogCriticalSection) )
        {
            if( (HardwareWatchDogTimeOut != 0.0) && HardwareWatchDogTimer.ExpiredSeconds(HardwareWatchDogTimeOut) && !HardwareWatchDogTimeOutFlag )
            {
                // The WatchDog has timed out.
                RobotFieldClearCritical();
                
                HardwareWatchDogTimeOutCount++;
                HardwareWatchDogTimeOutFlag = TRUE;
                
                // This is the old "Draconian" flag.
                //HardwareWatchDogTimedOut = TRUE;
            }

            LeaveCriticalSection(&HardwareWatchDogCriticalSection);
        }

        // Exit flag is passed as a pointer in the parameters.
        ExitFlag = *((BOOL *)parameters);

        if( ExitFlag || HardwareWatchDogTimedOut )
        {
            Exit = TRUE;
        }

        HardwareWatchDogLoopLatency.After();
    }

    mexPrintf("HardwareWatchDog shutting down (Exit=%s, TimeOut=%s)...\n",STR_YesNo(ExitFlag),STR_YesNo(HardwareWatchDogTimedOut));

    // Set running flag to FALSE.
    HardwareWatchDogRunning = FALSE;

    // Stop all hardware if the Watch-Dog has timed out...
    if( HardwareWatchDogTimedOut )
    {
        ok = HardwareStop();
    }

    return(0);
}

/******************************************************************************/

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
int func;
BOOL ok=TRUE;

    HardwareInit();

    // At least one input parameter required.
    if( nrhs == 0 )
    {
	    mexErrMsgTxt("mexRobotFunc: One or more input parameters required");
    }

    // Get the function code from the input parameter.
    func = (int)(*mxGetPr(prhs[0]));

    // Check range of function code.
    if( !((func >= 0) && (func < HARDWARE_MAX)) )
    {
	    mexErrMsgTxt("mexRobotFunc: Invalid function code");
    }

    // Number of input parameters determined by function code.
    if( (nrhs < HardwareFuncMinNRHS[func]) || (nrhs > HardwareFuncMaxNRHS[func]) )
    {
	    mexErrMsgTxt(STR_stringf("mexRobotFunc: From %d to %d input parameters required",HardwareFuncMinNRHS[func],HardwareFuncMaxNRHS[func]));
    }

    // Number of output parameters determined by function code.
    if( nlhs > HardwareFuncMaxNLHS[func] )
    {
	    mexErrMsgTxt(STR_stringf("mexRobotFunc: Maximum %d output parameters allowed",HardwareFuncMaxNLHS[func]));
    }

    // Check Watch-Dog timer.
    if( !HardwareWatchDogCheck(func) )
    {
        mexErrMsgTxt("mexRobotFunc: Watch-Dog timer expired (re-start required).\n");
        //mexPrintf("mexRobotFunc: Watch-Dog timer expired (re-start required).\n");
        ok = FALSE;
    }

    // Call the appropriate hardware function.
    if( ok )
    {
        ok = (*HardwareFunc[func])(nlhs,plhs,nrhs,prhs);
    }

    // If we're starting something, also start the Watch-Dog.
    if( ok && HardwareStartFuncFlag[func] )
    {
        ok = HardwareWatchDogStart();
    }

    // If we're stopping something, also stop the Watch-Dog.
    if( ok && HardwareStopFuncFlag[func] )
    {
        ok = HardwareWatchDogStop();
    }

    // Create 1st return paremter for Boolean success flag (ok).
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    (*mxGetPr(plhs[0])) = (double)ok;

    return;
}

/******************************************************************************/
