/******************************************************************************/

CRITICAL_SECTION RobotLoopCriticalSection;

#define ROBOTS 2
int     RobotCount=0;
STRING  RobotName[ROBOTS]={ "","" };
int     RobotID[ROBOTS]={ ROBOT_INVALID,ROBOT_INVALID };
int     RobotOther[ROBOTS] = { 1,0 };
double  RobotForceMax=40.0;

BOOL    RobotActive[ROBOTS]={ FALSE,FALSE };
BOOL    RobotJustActivated[ROBOTS]={ FALSE,FALSE };
BOOL    RobotJustDeactivated[ROBOTS]={ FALSE,FALSE };

double  RobotRampValue[ROBOTS]={ 0.0,0.0 };

matrix  RobotPosition[ROBOTS];
matrix  RobotVelocity[ROBOTS];
matrix  RobotForces[ROBOTS];

matrix  RobotControlPosition[ROBOTS];
matrix  RobotControlVelocity[ROBOTS];
matrix  RobotControlForces[ROBOTS];
matrix  RobotControlForcesLast[ROBOTS];
matrix  RobotControlForcesStep[ROBOTS];

#define ROBOT_FIELD_NONE                0
#define ROBOT_FIELD_VISCOUS             1
#define ROBOT_FIELD_SPRING              2
#define ROBOT_FIELD_CHANNEL             3
#define ROBOT_FIELD_PMOVE               4
#define ROBOT_FIELD_BIMANUAL_SPRING     5

char   *RobotFieldText[] = { "None","Viscous","Spring","Channel","PMove","BimanualSpring",NULL };
BOOL    RobotFieldBimanual[] = { FALSE,FALSE,FALSE,FALSE,FALSE,TRUE };
BOOL    RobotFieldNoRamp[] = { FALSE,FALSE,FALSE,FALSE,TRUE,FALSE };

int     RobotFieldType[ROBOTS];
matrix  RobotFieldForces[ROBOTS];

matrix  RobotViscousMatrix[ROBOTS];
double  RobotViscousSumMax=0.5;

matrix  RobotSpringHome[ROBOTS];
double  RobotSpringConstant[ROBOTS];  // N/cm
double  RobotDampingConstant[ROBOTS]; // N/cm/sec

matrix  RobotChannelStart[ROBOTS];
matrix  RobotChannelTarget[ROBOTS];
double  RobotChannelAngle[ROBOTS];

PMOVE   RobotPMove[ROBOTS];

#define ROBOT_BIMANUAL_SPRING_ALL     0 // Free to move in all axes.
#define ROBOT_BIMANUAL_SPRING_XCLAMP  1 // X clamped, others free to move.
#define ROBOT_BIMANUAL_SPRING_YCLAMP  2 // Y clamped, others free to move.

int     RobotBimanualSpringType=0;
double  RobotBimanualSpringSign[ROBOTS] = { 1,-1 };
matrix  RobotBimanualSpringOffset(3,1);

double  RobotPMoveMoveTime[ROBOTS];         // sec
double  RobotPMoveHoldTime[ROBOTS];         // sec
double  RobotPMoveRampTime=0.1;             // sec
double  RobotPMoveSpringConstant=-30.0;     // N/cm
double  RobotPMovePositionTolerance=0.2;    // cm
double  RobotPMoveVelocityTolerance=5.0;    // cm/sec
matrix  RobotPMoveTarget[ROBOTS];
int     RobotPMoveState[ROBOTS];
double  RobotPMoveStateTime[ROBOTS];
double  RobotPMoveStateRampValue[ROBOTS];
matrix  RobotPMoveStatePosition[ROBOTS];
BOOL    RobotPMoveFinished[ROBOTS];

double  RobotFieldRampValue[ROBOTS];
double  RobotFieldRampTime[ROBOTS];
RAMPER  RobotFieldRamp[ROBOTS];

BOOL    RobotFieldRampDownToNone[ROBOTS];
BOOL    RobotFieldDeactivate[ROBOTS] = { TRUE,TRUE };

BOOL    RobotLatestActive[ROBOTS];
double  RobotLatestRampValue[ROBOTS];
double  RobotLatestFieldRampValue[ROBOTS];
int     RobotLatestFieldType[ROBOTS];
BOOL    RobotLatestPMoveFinished[ROBOTS];
double  RobotLatestPosition[ROBOTS][3];
double  RobotLatestVelocity[ROBOTS][3];
double  RobotLatestForces[ROBOTS][3];

/******************************************************************************/

TIMER_Frequency RobotControlLoopFrequency("RobotControlLoop");
TIMER_Interval  RobotControlLoopLatency("RobotControlLoop");
TIMER_Interval  RobotForcesFunctionLatency("RobotForcesFunctionLoop");
TIMER_Frequency RobotNonControlLoopFrequency("RobotNonControlLoop");
TIMER_Interval  RobotNonControlLoopLatency("RobotNonControlLoop");

/******************************************************************************/

void RobotFieldClear( int robot )
{
    RobotFieldType[robot] = ROBOT_FIELD_NONE;
    RobotFieldRamp[robot].Zero();
}

/******************************************************************************/
    
void RobotFieldClear( void )
{
int robot;

    for( robot=0; (robot < RobotCount); robot++ )
    {
        RobotFieldClear(robot);
    }
}

/******************************************************************************/

void RobotFieldClearCritical( void )
{
    EnterCriticalSection(&RobotLoopCriticalSection);
    RobotFieldClear();
    LeaveCriticalSection(&RobotLoopCriticalSection);
}

/******************************************************************************/
    
BOOL RobotPMoveOpen( int robot )
{
BOOL ok;
matrix SC(3,1),PT(3,1),VT(3,1);

    SC(1,1) = RobotPMoveSpringConstant;
    SC(2,1) = RobotPMoveSpringConstant;
    SC(3,1) = 0.0;

    PT(1,1) = RobotPMovePositionTolerance;
    PT(2,1) = RobotPMovePositionTolerance;
    PT(3,1) = 0.0;

    VT(1,1) = RobotPMoveVelocityTolerance;
    VT(2,1) = RobotPMoveVelocityTolerance;
    VT(3,1) = 0.0;

    ok = RobotPMove[robot].Open(ROBOT_DOFS,RobotPMoveMoveTime[robot],SC,PT,VT,RobotPMoveHoldTime[robot],RobotPMoveRampTime);

    return(ok);
}

/******************************************************************************/

void RobotPMoveUpdate( int robot, matrix &F )
{
static matrix PMoveForces(3,1);
static BOOL finished;

    F.zeros();
    
    finished = RobotPMove[robot].Finished();

    if( RobotPMove[robot].Update(RobotPosition[robot],RobotVelocity[robot],PMoveForces) )
    {
        F(1,1) = PMoveForces(1,1);
        F(2,1) = PMoveForces(2,1);
        F(3,1) = 0.0;
    }

    RobotPMove[robot].CurrentState(RobotPMoveState[robot],RobotPMoveStateTime[robot],RobotPMoveStateRampValue[robot],RobotPMoveStatePosition[robot]);
    
    // Catch the PMove just as it finishes, clear the field, zero the ramp.
    if( RobotPMove[robot].Finished() && !finished )
    {
        RobotFieldClear(robot);
    }
}

/******************************************************************************/

void RobotForcesFunction( int robot, matrix &position, matrix &velocity, matrix &forces )
{
static matrix P(3,1),P1(3,1),P2(3,1),V(3,1),R(3,3),_R(3,3);
static int i;

    RobotForcesFunctionLatency.Before();
    
    RobotActive[robot] = ROBOT_Activated(RobotID[robot]);
    RobotJustActivated[robot] = ROBOT_JustActivated(RobotID[robot]);
    RobotJustDeactivated[robot] = ROBOT_JustDeactivated(RobotID[robot]);
    RobotRampValue[robot] = ROBOT_RampValue(RobotID[robot]);
    RobotFieldRampValue[robot] = RobotFieldRamp[robot].RampCurrent();
    RobotPMoveFinished[robot] = RobotPMove[robot].Finished();
    
    RobotPosition[robot] = position;
    RobotVelocity[robot] = velocity;
    
    RobotForces[robot].zeros();
    RobotFieldForces[robot].zeros();
    
    EnterCriticalSection(&RobotLoopCriticalSection);
    
    // Clear the current field if handle switch just released.
    if( RobotJustDeactivated[robot] && (RobotFieldDeactivate[robot] || (RobotFieldType[robot] == ROBOT_FIELD_PMOVE)) )
    {
        // Clear the field for the other robot if bimanual field.
        if( RobotFieldBimanual[RobotFieldType[robot]] )
        {
            RobotFieldClear(RobotOther[robot]);
        }

        // Abort PMove (ignores request if PMove not in progress).
        RobotPMove[robot].Abort();
        
        RobotFieldClear(robot);
    }
        
    // Turn field off if ramp-down complete...
    if( RobotFieldRampDownToNone[robot] && (RobotFieldType[robot] != ROBOT_FIELD_NONE) && RobotFieldRamp[robot].RampComplete() && (RobotFieldRamp[robot].RampCurrent() == 0.0) )
    {
        RobotFieldRampDownToNone[robot] = FALSE;
        RobotFieldType[robot] = ROBOT_FIELD_NONE;
    }
    
    switch( RobotFieldType[robot] )
    {
        case ROBOT_FIELD_NONE :
            break;
            
        case ROBOT_FIELD_VISCOUS :
            RobotFieldForces[robot] = RobotViscousMatrix[robot] * RobotVelocity[robot];
            break;
            
        case ROBOT_FIELD_SPRING :
            RobotFieldForces[robot] = RobotSpringConstant[robot] * (RobotPosition[robot] - RobotSpringHome[robot]);
            RobotFieldForces[robot] += RobotDampingConstant[robot] * RobotVelocity[robot];
            break;
            
        case ROBOT_FIELD_CHANNEL :
            SPMX_romxZ(D2R(RobotChannelAngle[robot]),R);
            SPMX_romxZ(D2R(-RobotChannelAngle[robot]),_R);
            
            P = R * (RobotPosition[robot] - RobotChannelStart[robot]);
            V = R * RobotVelocity[robot];
            
            // Calculate perpendicular (X) channel forces.
            RobotFieldForces[robot](1,1) = (RobotSpringConstant[robot] * P(1,1)) + (RobotDampingConstant[robot] * V(1,1));
            
            // Rotate back to original.
            RobotFieldForces[robot] = _R * RobotFieldForces[robot];
            break;
            
        case ROBOT_FIELD_PMOVE :
            // When PMove done, this also will clear field and zero ramp.
            RobotPMoveUpdate(robot,RobotFieldForces[robot]);
            break;
            
        case ROBOT_FIELD_BIMANUAL_SPRING :
            P1 = RobotPosition[robot];
            P2 = RobotPosition[RobotOther[robot]] + (RobotBimanualSpringSign[robot] * RobotBimanualSpringOffset);
            
            RobotFieldForces[robot] = RobotSpringConstant[robot] * (P1 - P2);
            
            if( RobotBimanualSpringType != ROBOT_BIMANUAL_SPRING_ALL )
            {
                i = RobotBimanualSpringType;
                RobotFieldForces[robot](i,1) = RobotSpringConstant[robot] * (RobotPosition[robot](i,1) - RobotSpringHome[robot](i,1));
            }
            
            RobotFieldForces[robot] += RobotDampingConstant[robot] * RobotVelocity[robot];
            break;
    }
    
    LeaveCriticalSection(&RobotLoopCriticalSection);
    
    if( RobotActive[robot] && (RobotFieldBimanual[RobotFieldType[robot]] ? RobotActive[RobotOther[robot]] : TRUE) && !HardwareWatchDogTimedOut )
    {
        RobotForces[robot] = RobotFieldRampValue[robot] * RobotFieldForces[robot];
        RobotForces[robot].clampnorm(RobotForceMax);
        
        forces = RobotForces[robot];
    }
    
    RobotForcesFunctionLatency.After();
}

/******************************************************************************/

void RobotControlLoop( void )
{
static int robot;
static matrix A(3,1),F(3,1);

    RobotControlLoopFrequency.Loop();
    
    RobotControlLoopLatency.Before();
    
    // Loop over robots to get position, etc.
    for( robot=0; (robot < RobotCount); robot++ )
    {
        ROBOT_Position(RobotID[robot],RobotControlPosition[robot],RobotControlVelocity[robot],A);
    }
    
    // Loop over robots to calculate the forces.
    for( robot=0; (robot < RobotCount); robot++ )
    {
        RobotControlForces[robot].zeros();
        
        RobotForcesFunction(robot,RobotControlPosition[robot],RobotControlVelocity[robot],RobotControlForces[robot]);

        // Monitor how much forces have increased since the last time-step.
        if( norm(RobotControlForces[robot]) > norm(RobotControlForcesLast[robot]) )
        {
            RobotControlForcesStep[robot] = RobotControlForces[robot] - RobotControlForcesLast[robot];
        }
        
        RobotControlForcesLast[robot] = RobotControlForces[robot];
    }
    
    // Loop over robots to set the forces.
    for( robot=0; (robot < RobotCount); robot++ )
    {
        ROBOT_Force(RobotID[robot],RobotControlForces[robot]);
    }
    
    RobotControlLoopLatency.After();
}

/******************************************************************************/

void RobotNonControlLoop( void )
{
static int robot,i;

    RobotNonControlLoopFrequency.Loop();
    
    RobotNonControlLoopLatency.Before();
    
    EnterCriticalSection(&RobotLoopCriticalSection);
    
    for( robot=0; (robot < RobotCount); robot++ )
    {
        RobotLatestActive[robot] = RobotActive[robot];
        RobotLatestRampValue[robot] = RobotRampValue[robot];
        RobotLatestFieldRampValue[robot] = RobotFieldRampValue[robot];
        RobotLatestFieldType[robot] = RobotFieldType[robot];
        RobotLatestPMoveFinished[robot] = RobotPMoveFinished[robot];

        for( i=0; (i < 3); i++ )
        {
            RobotLatestPosition[robot][i] = RobotPosition[robot](i+1,1);
            RobotLatestVelocity[robot][i] = RobotVelocity[robot](i+1,1);
            RobotLatestForces[robot][i] = RobotForces[robot](i+1,1);
        }
    }
    
    LeaveCriticalSection(&RobotLoopCriticalSection);
    
    RobotNonControlLoopLatency.After();
}

/******************************************************************************/

void RobotTimerResults( void )
{
    // Print results of timers...
    RobotControlLoopFrequency.Results(mexPrintf);
    RobotControlLoopLatency.Results(mexPrintf);
    
    RobotForcesFunctionLatency.Results(mexPrintf);
            
    RobotNonControlLoopFrequency.Results(mexPrintf);
    RobotNonControlLoopLatency.Results(mexPrintf);
    
    // Reset timers...
    RobotControlLoopFrequency.Reset();
    RobotControlLoopLatency.Reset();
    
    RobotForcesFunctionLatency.Reset();
    
    RobotNonControlLoopFrequency.Reset();
    RobotNonControlLoopLatency.Reset();
}

/******************************************************************************/

BOOL RobotStarted( void )
{
BOOL flag;
int robot;

    for( flag=FALSE,robot=0; (robot < ROBOTS); robot++ )
    {
        if( RobotID[robot] != ROBOT_INVALID )
        {
            flag = TRUE;
        }
    }

    return(flag);
}

/******************************************************************************/

BOOL RobotStop( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE,flag=FALSE;
int robot;

    if( RobotStarted() )
    {
        mexPrintf("mexRobotFunc: Robot(s) stopping...\n");
        flag = TRUE;
    }

    LOOPTASK_stop(RobotNonControlLoop);
    LOOPTASK_stop(RobotControlLoop);

    for( robot=0; (robot < ROBOTS); robot++ )
    {
        if( RobotID[robot] != ROBOT_INVALID )
        {
            ROBOT_Stop(RobotID[robot]);
            ROBOT_Close(RobotID[robot]);
            
            RobotID[robot] = ROBOT_INVALID;
        }
        
        RobotFieldRamp[robot].Stop();
        RobotFieldRamp[robot].Close();
    }
    
    RobotCount = 0;

    if( flag )
    {
        RobotTimerResults();
    }
    
    return(ok);
}

/******************************************************************************/

BOOL RobotStopCritical( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok;

    EnterCriticalSection(&HardwareWatchDogCriticalSection);
    ok = RobotStop(nlhs,plhs,nrhs,prhs);
    LeaveCriticalSection(&HardwareWatchDogCriticalSection);
    
    return(ok);
}

/******************************************************************************/

BOOL RobotStart( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int robot;
char *sptr;

    if( RobotStarted() )
    {
        mexPrintf("mexRobotFunc: Robot already started.\n");
        return(TRUE);
    }

    // Clear some variables.
    for( robot=0; (robot < ROBOTS); robot++ )
    {
        memset(RobotName[robot],0,STRLEN);
        RobotFieldType[robot] = ROBOT_FIELD_NONE;
        RobotFieldRampDownToNone[robot] = FALSE;
    }    

    // Count the number of robots specified.
    for( RobotCount=0; (RobotCount < ROBOTS); RobotCount++ )
    {
        if( RobotCount == (nrhs-1) )
        {
            break;
        }
        
        if( (sptr=mxArrayToString(prhs[RobotCount+1])) != NULL )
        {
            strncpy(RobotName[RobotCount],sptr,STRLEN);
        }
    
        if( STR_null(RobotName[RobotCount]) )
        {
            break;
        }
    }

    if( RobotCount == 0 )
    {
        mexPrintf("mexRobotFunc: Robot name(s) not specified.\n");
        return(FALSE);
    }

    mexPrintf("mexRobotFunc: Robot(s) starting...\n");
    
    for( robot=0; ((robot < RobotCount) && ok); robot++ )
    {
        if( (RobotID[robot]=ROBOT_Open(RobotName[robot])) == ROBOT_INVALID )
        {
            mexPrintf("mexRobotFunc: %s open failed.\n",RobotName[robot]);
            ok = FALSE;
        }
        else
        if( !ROBOT_Start(RobotID[robot]) )
        {
            mexPrintf("mexRobotFunc: %s start failed.\n",RobotName[robot]);
            ok = FALSE;
        }
        else
        if( !RobotFieldRamp[robot].Start(RobotFieldRampTime[robot]) )
        {
            mexPrintf("mexRobotFunc: Cannot start ramper.\n");
            ok = FALSE;
        }
        else
        if( !RobotPMoveOpen(robot) )
        {
            mexPrintf("mexRobotFunc: Cannot open PMove object.\n");
            ok = FALSE;
        }
        else
        {
            RobotFieldRamp[robot].Zero();  // Rampers seem to start at one (hideous!).
            mexPrintf("mexRobotFunc: %s started.\n",RobotName[robot]);
        }
    }

    if( !ok )
    {
        ok = RobotStopCritical(0,NULL,0,NULL);
        return(FALSE);
    }
            
    if( !LOOPTASK_start(RobotControlLoop) )
    {
        mexPrintf("mexRobotFunc: Cannot start robot control loop.\n");
        ok = FALSE;
    }
    else
    if( !LOOPTASK_start(RobotNonControlLoop) )
    {
        mexPrintf("mexRobotFunc: Cannot start robot non-control loop.\n");
        ok = FALSE;
    }
    else    
    /*if( !LOOPTASK_start(HardwareDataLoop) )
    {
        mexPrintf("mexRobotFunc: Cannot start hardware data loop.\n");
        ok = FALSE;
    }*/

    if( !ok )
    {
        ok = RobotStopCritical(0,NULL,0,NULL);
        return(FALSE);
    }
    
    mexPrintf("mexRobotFunc: Robot(s) started.\n");
    
    return(ok);
}

/******************************************************************************/

BOOL RobotFieldCheck( int robot )
{
BOOL ok=TRUE;

    if( !RobotStarted() )
    {
        return(FALSE);
    }

    //mexPrintf("RobotFieldCheck(robot=%d) RampDownToNone=%s FieldType=%d\n",robot,STR_YesNo(RobotFieldRampDownToNone[robot]),RobotFieldType[robot]);
    
    if( (RobotFieldRamp[robot].RampCurrent() != 0.0) || (RobotFieldType[robot] != ROBOT_FIELD_NONE) )
    {
        mexPrintf("mexRobotFunc: Ramp must be zero and field must be cleared.\n");
        //ok = FALSE;
    }
    
    return(ok);
}

/******************************************************************************/

BOOL RobotFieldCheck( void )
{
int robot;
BOOL ok;

    for( ok=TRUE,robot=0; ((robot < RobotCount) && ok); robot++ )
    {
        ok = RobotFieldCheck(robot);
    }

    return(ok);
}

/******************************************************************************/

int RobotIndex( int nrhs, const mxArray *prhs[], int &robot )
{
int j=-1;

    if( RobotCount == 1 )
    {
        robot = 0;
        j = 1;
    }
    else
    if( RobotCount == 2 )
    {
        j = 1;
        
        MexVarToInt(prhs[j++],robot);
        if( (robot < 0) || (robot >= ROBOTS) )
        {
            robot = 0;
            j = -1;
        }
    }

    //mexPrintf("RobotIndex(...) robot=%d, j=%d\n",robot,j);
    
    return(j);
}

/******************************************************************************/

BOOL RobotFieldRampUp( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int robot=0;
int j;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    EnterCriticalSection(&RobotLoopCriticalSection);
    
    RobotFieldRamp[robot].Up();
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotFieldRampDown( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int robot=0;
int j;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotFieldRamp[robot].Down();
    RobotFieldRampDownToNone[robot] = TRUE;
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotFieldNone( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int robot=0;
int j;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    // Clearing the field is a special case which we do without checking...
    /*if( !RobotFieldCheck(robot) )
    {
        return(FALSE);
    }*/

    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotFieldClear(robot);
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotFieldViscous( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
static matrix ViscousMatrix(3,3);
BOOL ok=TRUE;
int robot=0;
int i,j;
double sum;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    if( !RobotFieldCheck(robot) )
    {
        return(FALSE);
    }

    // Process field parameters...
    if( !MexVarToMatrix(prhs[j++],ViscousMatrix,TRUE) )
    {
        mexPrintf("mexRobotFunc: Invalid dimensions for viscous field matrix.\n");
        return(ok);
    }

    for( sum=0.0,i=1; (i <= ViscousMatrix.cols()); i++ )
    {
        for( j=1; (j <= ViscousMatrix.rows()); j++ )
        {
            sum = sum + fabs(ViscousMatrix(i,j));
        }
    }
    
    // Check safe value of field matrix.
    if( sum > RobotViscousSumMax )
    {
        mexPrintf("mexRobotFunc: Invalid values for viscous field matrix (sum=%.1lf).\n",sum);
        return(FALSE);
    }
    
    mexPrintf("Field=VISCOUS, Viscous=%.2lf(N/cm/sec)\n",sum);
            
    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotFieldType[robot] = ROBOT_FIELD_VISCOUS;
    
    RobotViscousMatrix[robot] = ViscousMatrix;
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotFieldSpring( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
static matrix SpringHome(3,1);
double SpringConstant=0.0,DampingConstant=0.0;
BOOL ok=TRUE;
int robot=0;
int j;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    if( !RobotFieldCheck(robot) )
    {
        return(FALSE);
    }

    // Process field parameters...
    if( !MexVarToMatrix(prhs[j++],SpringHome,TRUE) )
    {
        mexPrintf("mexRobotFunc: Invalid dimensions for spring home position.\n");
        return(ok);
    }

    MexVarToDouble(prhs[j++],SpringConstant);
    MexVarToDouble(prhs[j++],DampingConstant);

    mexPrintf("Field=SPRING, Spring=%.2lf(N/cm), Damping=%.2lf(N/cm/sec)\n",SpringConstant,DampingConstant);

    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotFieldType[robot] = ROBOT_FIELD_SPRING;
    
    RobotSpringHome[robot] = SpringHome;
    RobotSpringConstant[robot] = SpringConstant;
    RobotDampingConstant[robot] = DampingConstant;
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotFieldChannel( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
static matrix ChannelStart(3,1),ChannelTarget(3,1),D(3,1);
double SpringConstant=0.0,DampingConstant=0.0,ChannelAngle=0.0;
BOOL ok=TRUE;
int robot=0;
int j;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    if( !RobotFieldCheck(robot) )
    {
        return(FALSE);
    }

    // Process field parameters...
    /*if( !MexVarToMatrix(prhs[j++],ChannelStart) )
    {
        mexPrintf("mexRobotFunc: Invalid dimensions for channel start position.\n");
        return(ok);
    }*/

    if( !MexVarToMatrix(prhs[j++],ChannelTarget,TRUE) )
    {
        mexPrintf("mexRobotFunc: Invalid dimensions for channel target position.\n");
        return(ok);
    }

    //mexPrintf("ChannelTarget = %.2lf,%.2lf,%.2lf\n",ChannelTarget(1,1),ChannelTarget(2,1),ChannelTarget(3,1));
    MexVarToDouble(prhs[j++],SpringConstant);
    MexVarToDouble(prhs[j++],DampingConstant);
    
    ChannelStart = RobotPosition[robot];
    D = ChannelTarget - ChannelStart;
    ChannelAngle = R2D(atan2(D(1,1),D(2,1)));
    //mexPrintf("D = %.2lf,%.2lf\n",D(1,1),D(2,1));
    
    mexPrintf("Field=CHANNEL, Spring=%.2lf(N/cm), Damping=%.2lf(N/cm/sec), Angle=%.2lf(deg)\n",SpringConstant,DampingConstant,ChannelAngle);

    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotFieldType[robot] = ROBOT_FIELD_CHANNEL;
    
    RobotChannelStart[robot] = ChannelStart;
    RobotChannelTarget[robot] = ChannelTarget;
    
    RobotChannelAngle[robot] = ChannelAngle;
    
    RobotSpringConstant[robot] = SpringConstant;
    RobotDampingConstant[robot] = DampingConstant;
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotFieldPMove( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
static matrix TargetPosition(3,1),D;
double MoveTime=0.0,HoldTime=0.0,d;
BOOL ok=TRUE;
int robot=0;
int j;

    // Get robot index from input parameters, if required.
    if( (j=RobotIndex(nrhs,prhs,robot)) == -1 )
    {
        return(FALSE);
    }
    
    if( !RobotFieldCheck(robot) )
    {
        return(FALSE);
    }

    // Process field parameters...
    if( !MexVarToMatrix(prhs[j++],TargetPosition,TRUE) )
    {
        mexPrintf("mexRobotFunc: Invalid dimensions for PMove target position.\n");
        return(ok);
    }
    
    D = TargetPosition - RobotPosition[robot];
    d = norm(D);

    MexVarToDouble(prhs[j++],MoveTime);
    MexVarToDouble(prhs[j++],HoldTime);
    
    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotPMoveTarget[robot] = TargetPosition;
    RobotPMoveMoveTime[robot] = MoveTime;
    RobotPMoveHoldTime[robot] = HoldTime;

    if( (ok=RobotPMove[robot].Start(RobotPosition[robot],RobotPMoveTarget[robot],RobotPMoveMoveTime[robot],RobotPMoveHoldTime[robot])) )
    {
        RobotFieldType[robot] = ROBOT_FIELD_PMOVE;
    }
    
    // Ramp imediately to one.
    RobotFieldRamp[robot].One();
    
    LeaveCriticalSection(&RobotLoopCriticalSection);
    
    mexPrintf("Field=PMOVE, Distance=%.1f(cm), MoveTime=%.3lf(sec), HoldTime=%.3lf(sec), %s\n",d,MoveTime,HoldTime,STR_OkFailed(ok));
    
    return(ok);
}

/******************************************************************************/

BOOL RobotFieldBimanualSpring( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
static matrix BimanualOffset(3,1);
double SpringConstant=0.0,DampingConstant=0.0,d;
int BimanualSpringType=0;
BOOL ok=TRUE;
int robot=0;
int j;

    if( RobotCount != 2 )
    {
        mexPrintf("mexRobotFunc: Bimanual robots required for this field.\n");
        return(FALSE);
    }

    if( !RobotFieldCheck() )
    {
        return(FALSE);
    }

    // Process field parameters...
    j = 1;
    MexVarToInt(prhs[j++],BimanualSpringType);
    MexVarToDouble(prhs[j++],SpringConstant);
    MexVarToDouble(prhs[j++],DampingConstant);
    
    d = norm(RobotPosition[0] - RobotPosition[1]);

    mexPrintf("Field=BIMANUAL-SPRING, Type=%d, Spring=%.2lf(N/cm), Damping=%.2lf(N/cm/sec), d=%.1lf(cm)\n",BimanualSpringType,SpringConstant,DampingConstant,d);

    EnterCriticalSection(&RobotLoopCriticalSection);

    RobotBimanualSpringType = BimanualSpringType;
    RobotBimanualSpringOffset = RobotPosition[0] - RobotPosition[1];
    
    for( robot=0; (robot < RobotCount); robot++ )
    {
        RobotFieldType[robot] = ROBOT_FIELD_BIMANUAL_SPRING;
    
        RobotSpringHome[robot] = RobotPosition[robot];
        RobotSpringConstant[robot] = SpringConstant;
        RobotDampingConstant[robot] = DampingConstant;
    }
    
    LeaveCriticalSection(&RobotLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL RobotGetLatest( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int robot,i,j,k;
double dtrash[3];
double *RS,*RP,*RV,*RF;

    if( !RobotStarted() )
    {
        mexPrintf("mexRobotFunc: Robot(s) not started.\n");
        return(FALSE);
    }

    // Point to trash variables in case not specified.
    RS = dtrash;
    RP = dtrash;
    RV = dtrash;
    RF = dtrash;

    j = 2;
    if( nlhs >= j ) // Robot status (active flag, ramp values, etc)
    {               
        plhs[j-1] = mxCreateDoubleMatrix(5,RobotCount,mxREAL); 
        RS = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Robot position.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(3,RobotCount,mxREAL); 
        RP = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Robot velocity.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(3,RobotCount,mxREAL); 
        RV = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Robot force.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(3,RobotCount,mxREAL); 
        RF = mxGetPr(plhs[j-1]);
    }

    EnterCriticalSection(&RobotLoopCriticalSection);

    for( robot=0; (robot < RobotCount); robot++ )
    {
        // Robot status.
        k = (robot*5);
        RS[k++] = (double)RobotLatestActive[robot];
        RS[k++] = RobotLatestRampValue[robot];
        RS[k++] = RobotLatestFieldRampValue[robot];
        RS[k++] = (double)RobotLatestFieldType[robot];
        RS[k++] = (double)RobotLatestPMoveFinished[robot];

        // Robot position, velocity, etc.
        for( i=0; (i < 3); i++ )
        {
            k = (robot*3)+i;
            RP[k] = RobotLatestPosition[robot][i];
            RV[k] = RobotLatestVelocity[robot][i];
            RF[k] = RobotLatestForces[robot][i];
        }
    }
    
    LeaveCriticalSection(&RobotLoopCriticalSection);
    
    return(ok);
}

/******************************************************************************/

