/******************************************************************************/

#define GRAPHICS_CONFIG           "GRAPHICS.CFG"
#define GRAPHICS_CALIBRATION      "GRAPHICS_%dD.CAL"

#define GL_ROTATE_X     1.0,0.0,0.0    // OpenGL rotation axis...
#define GL_ROTATE_Y     0.0,1.0,0.0
#define GL_ROTATE_Z     0.0,0.0,1.0

/******************************************************************************/

#define GRAPHICS_errorf printf
#define GRAPHICS_debugf printf
#define GRAPHICS_messgf printf

/******************************************************************************/

#define OPENGL_VECTOR   16
#define OPENGL_MATRIX   4,4

/******************************************************************************/

#define GRAPHICS_RANGE  2
#define GRAPHICS_MIN    0
#define GRAPHICS_MAX    1

#define GRAPHICS_X      0
#define GRAPHICS_Y      1
#define GRAPHICS_Z      2

#define GRAPHICS_2D     2
#define GRAPHICS_3D     3
#define GRAPHICS_RGB    3

/******************************************************************************/

#define GRAPHICS_WINDOW_INVALID -1

/******************************************************************************/

#define GRAPHICS_CONFIG           "GRAPHICS.CFG"
#define GRAPHICS_CALIBRATION      "GRAPHICS_%dD.CAL"

/******************************************************************************/

extern  STRING  GRAPHICS_Description;
extern  int     GRAPHICS_StereoMode;
extern  float   GRAPHICS_DisplaySize[GRAPHICS_3D][GRAPHICS_RANGE];
extern  float   GRAPHICS_FocalPlane;
extern  float   GRAPHICS_PupilToCentre;
extern  float   GRAPHICS_EyeCentre[GRAPHICS_3D];
extern  matrix  GRAPHICS_EyePOMX;
extern  STRING  GRAPHICS_CalibPath;
extern  STRING  GRAPHICS_CalibName;
extern  BOOL    GRAPHICS_SpaceBall;
extern  float   GRAPHICS_DisplayRotation[GRAPHICS_3D];
extern  int     GRAPHICS_SphereSegments;

/******************************************************************************/

extern  struct  STR_TextItem  GRAPHICS_DisplayText[];
extern  struct  STR_TextItem  GRAPHICS_StereoText[];

/******************************************************************************/

extern  HWND    GRAPHICS_WindowParent;
extern  HWND    GRAPHICS_WindowGLUT;

extern  int     GRAPHICS_Display;
extern  int     GRAPHICS_Dimensions;

extern  int     GRAPHICS_DisplayPixels[GRAPHICS_2D];
extern  int     GRAPHICS_DisplayHeight;
extern  int     GRAPHICS_DisplayFrequency;

extern  double  GRAPHICS_VerticalRetraceFrequency;
extern  double  GRAPHICS_VerticalRetracePeriod;
extern  long    GRAPHICS_SwapBuffersCount;
extern  double  GRAPHICS_SwapBuffersToVerticalRetraceTime;
extern  double  GRAPHICS_SwapBuffersLastOnsetTime;
extern  double  GRAPHICS_SwapBuffersLastOffsetTime;
extern  double  GRAPHICS_VerticalRetraceNextOnsetTime;
extern  double  GRAPHICS_VerticalRetraceNextOffsetTime;

/******************************************************************************/

// Screen dimensions...
#define SL      GRAPHICS_DisplaySize[GRAPHICS_X][GRAPHICS_MIN]
#define SR      GRAPHICS_DisplaySize[GRAPHICS_X][GRAPHICS_MAX]
#define ST      GRAPHICS_DisplaySize[GRAPHICS_Y][GRAPHICS_MAX]
#define SB      GRAPHICS_DisplaySize[GRAPHICS_Y][GRAPHICS_MIN]
#define SZ      GRAPHICS_FocalPlane 

#define ZNEAR   GRAPHICS_DisplaySize[GRAPHICS_Z][GRAPHICS_MIN]
#define ZFAR    GRAPHICS_DisplaySize[GRAPHICS_Z][GRAPHICS_MAX]

// Eye position...
#define EX      GRAPHICS_EyeCentre[GRAPHICS_X]
#define EY      GRAPHICS_EyeCentre[GRAPHICS_Y]
#define EZ      GRAPHICS_EyeCentre[GRAPHICS_Z]
#define ESEP    GRAPHICS_PupilToCentre

// Screen resolution (pixels)...
#define HPIX    GRAPHICS_DisplayPixels[GRAPHICS_X]
#define VPIX    GRAPHICS_DisplayPixels[GRAPHICS_Y]

// Workspace dimensions...
#define WL      GRAPHICS_CalibRange[GRAPHICS_X][GRAPHICS_MIN]
#define WR      GRAPHICS_CalibRange[GRAPHICS_X][GRAPHICS_MAX]
#define WT      GRAPHICS_CalibRange[GRAPHICS_Y][GRAPHICS_MAX]
#define WB      GRAPHICS_CalibRange[GRAPHICS_Y][GRAPHICS_MIN]
#define WN      GRAPHICS_CalibRange[GRAPHICS_Z][GRAPHICS_MIN]
#define WF      GRAPHICS_CalibRange[GRAPHICS_Z][GRAPHICS_MIN]

/******************************************************************************/

#define GRAPHICS_SHOWMOUSE     0x10    // Show mouse cursor in GLUT window.
#define GRAPHICS_DONTFOCUS     0x20    // Don't mess with window focus.
#define GRAPHICS_DISPLAY       0x0F    // Mask for display bits (see below)...

#define GRAPHICS_DISPLAY_DEFAULT 0x0F  // Default (user specified).
#define GRAPHICS_DISPLAY_MONO    0x00  // Regular 3D
#define GRAPHICS_DISPLAY_STEREO  0x01  // Stereoscopic 3D
#define GRAPHICS_DISPLAY_2D      0x02  // 2D

/******************************************************************************/

#define GRAPHICS_STEREO_NONE           0
#define GRAPHICS_STEREO_FRAMEALTERNATE 1
#define GRAPHICS_STEREO_DUALSCREEN     2
#define GRAPHICS_STEREO_OCULUS         3

/******************************************************************************/

#define EYE_MAX    3
#define EYE_LEFT   0
#define EYE_RIGHT  1
#define EYE_MONO   2


// Color table { red,green,blue,alpha,pname } values
#define GRAPHICS_COLOR_LIGHTBLUE   0.30f,0.30f,0.70f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_DARKBLUE    0.15f,0.15f,0.35f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_BLACK       0.00f,0.00f,0.00f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_LIGHTBLACK  0.10f,0.10f,0.10f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_RED         1.00f,0.00f,0.00f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_YELLOW      1.00f,0.90f,0.00f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_BLUE        0.00f,0.00f,1.00f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_GREEN       0.00f,0.80f,0.00f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_WHITE       1.00f,1.00f,1.00f,1.00f,GL_AMBIENT_AND_DIFFUSE
#define GRAPHICS_COLOR_GREY        0.50f,0.50f,0.50f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_DARKGREY    0.25f,0.25f,0.25f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_LIGHTGREY   0.75f,0.75f,0.75f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_TURQUOISE   0.30f,1.00f,1.00f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_PURPLE      1.00f,0.00f,1.00f,1.00f,GL_AMBIENT
#define GRAPHICS_COLOR_UNDEFINED  -1.0f,-1.0f,-1.0f,-1.0f,-1.0f

// Color table row numbers.
#define NOCOLOR    -1
#define RED         0
#define YELLOW      1
#define BLUE        2
#define WHITE       3
#define GREEN       4
#define TURQUOISE   5
#define PURPLE      6
#define BLACK       7
#define LIGHTBLUE   8
#define DARKBLUE    9
#define GREY       10
#define DARKGREY   11
#define LIGHTBLACK 12
#define LIGHTGREY  13

#define GRAPHICS_COLOR_ROWS       32
#define GRAPHICS_COLOR_COLUMNS    5
#define GRAPHICS_COLOR_VALUES     GRAPHICS_COLOR_COLUMNS
#define GRAPHICS_COLOR_RGB        3
#define GRAPHICS_COLOR_R          0
#define GRAPHICS_COLOR_G          1
#define GRAPHICS_COLOR_B          2
#define GRAPHICS_COLOR_ALPHA      3
#define GRAPHICS_COLOR_PNAME      4

/******************************************************************************/

struct  STR_TextItem  GRAPHICS_DisplayText[] =
{
    { GRAPHICS_DISPLAY_MONO   ,"3D-MONO"   },
    { GRAPHICS_DISPLAY_STEREO ,"3D-STEREO" },
    { GRAPHICS_DISPLAY_2D     ,"2D"        },
    { GRAPHICS_DISPLAY_DEFAULT,"DEFAULT"   },
    { GRAPHICS_DISPLAY_MONO   ,"MONO"      },
    { GRAPHICS_DISPLAY_STEREO ,"STEREO"    },
    { STR_TEXT_ENDOFTABLE },
};

/******************************************************************************/

struct  STR_TextItem  GRAPHICS_StereoText[] =
{
    { GRAPHICS_STEREO_NONE          ,""                 },
    { GRAPHICS_STEREO_NONE          ,"None"             },
    { GRAPHICS_STEREO_FRAMEALTERNATE,"FrameAlternation" },
    { GRAPHICS_STEREO_DUALSCREEN    ,"DualScreen"       },
    { GRAPHICS_STEREO_OCULUS        ,"OculusHMD"        },
    { STR_TEXT_ENDOFTABLE },
};

/******************************************************************************/

BOOL GRAPHICS_DisplayModeParse( int &code, char *text )
{
BOOL ok=FALSE;

    ok = STR_TextCode(GRAPHICS_DisplayText,code,text);

    return(ok);
}

/******************************************************************************/

BOOL GRAPHICS_StereoModeParse( int &code, char *text )
{
BOOL ok=FALSE;

    ok = STR_TextCode(GRAPHICS_StereoText,code,text);

    return(ok);
}

/******************************************************************************/

BOOL GRAPHICS_StereoModeOculus( void )
{
BOOL flag;

    flag = (GRAPHICS_StereoMode == GRAPHICS_STEREO_OCULUS);

    return(flag);
}

/******************************************************************************/

STRING   GRAPHICS_Config=GRAPHICS_CONFIG;
int      GRAPHICS_WindowID=GRAPHICS_WINDOW_INVALID;

STRING   GRAPHICS_Description="Graphics Window";
int      GRAPHICS_DisplayPixels[GRAPHICS_2D] = { 0,0 };
int      GRAPHICS_DisplayFrequency = 0;
float    GRAPHICS_DisplayRotation[GRAPHICS_3D] = { 0.0,0.0,0.0 };
STRING   GRAPHICS_StereoModeString="NONE";
int      GRAPHICS_StereoMode=GRAPHICS_STEREO_NONE;
STRING   GRAPHICS_DefaultModeString="2D";
int      GRAPHICS_DefaultMode=GRAPHICS_DISPLAY_2D;
float    GRAPHICS_DisplaySize[GRAPHICS_3D][GRAPHICS_RANGE];
float    GRAPHICS_FocalPlane=0;
float    GRAPHICS_PupilToCentre=3.0;
float    GRAPHICS_EyeCentre[GRAPHICS_3D]= { 0.0,0.0,50.0 };
double   GRAPHICS_LightPosition[GRAPHICS_3D] = { 0.0,0.0,0.0 };
double   GRAPHICS_LightColor[GRAPHICS_RGB] = { 1.0,1.0,1.0 };
STRING   GRAPHICS_CalibrationFile=GRAPHICS_CALIBRATION;
BOOL     GRAPHICS_SpaceBall=FALSE;
BOOL     GRAPHICS_StereoDepth=TRUE;
int      GRAPHICS_ClearColorDefault=BLACK;
STRING   GRAPHICS_ClearColorString="BLACK";
double   GRAPHICS_DisplayDelayMinimum=0.0;
int      GRAPHICS_SphereSegments=100;
int      GRAPHICS_WireSphereSegments=20;
matrix   GRAPHICS_EyeCentrePOMX;
BOOL     GRAPHICS_DoubleBufferedFlag=TRUE;
BYTE     GRAPHICS_FlagOpenGL=0x00;

int      GRAPHICS_DefaultPixels[GRAPHICS_2D];
int      GRAPHICS_DefaultFrequency;

double   GRAPHICS_VerticalRetraceFrequency;
double   GRAPHICS_VerticalRetracePeriod;
TIMER    GRAPHICS_VerticalRetraceOnsetTimer("GRAPHICS_VerticalRetraceOnsetTimer");
TIMER    GRAPHICS_VerticalRetraceOffsetTimer("GRAPHICS_VerticalRetraceOffsetTimer");
TIMER    GRAPHICS_VerticalRetraceCatchTimer("GRAPHICS_VerticalRetraceCatchTimer");
double   GRAPHICS_VerticalRetraceCatchTime = 0.050; // msec
TIMER    GRAPHICS_VerticalRetraceTimer("GRAPHICS_VerticalRetraceTimer");
BOOL     GRAPHICS_VerticalRetraceOnsetSyncFlag = FALSE;
BOOL     GRAPHICS_VerticalRetraceOffsetSyncFlag = FALSE;

DATAPROC GRAPHICS_VerticalRetraceOnsetTimeData("GRAPHICS_VerticalRetraceOnsetTime");
DATAPROC GRAPHICS_VerticalRetraceOnsetErrorData("GRAPHICS_VerticalRetraceOnsetError");
DATAPROC GRAPHICS_VerticalRetraceOffsetTimeData("GRAPHICS_VerticalRetraceOffsetTime");
DATAPROC GRAPHICS_VerticalRetraceOffsetErrorData("GRAPHICS_VerticalRetraceOffsetError");
DATAPROC GRAPHICS_SwapBuffersToVerticalRetraceTimeData("GRAPHICS_SwapBuffersToVerticalRetraceTime",200000);

long     GRAPHICS_SwapBuffersCount=0;
double   GRAPHICS_SwapBuffersToVerticalRetraceTime;
double   GRAPHICS_SwapBuffersLastOnsetTime;
double   GRAPHICS_SwapBuffersLastOffsetTime;
double   GRAPHICS_VerticalRetraceNextOnsetTime;
double   GRAPHICS_VerticalRetraceNextOffsetTime;

/******************************************************************************/

TIMER_Interval   GRAPHICS_ClearStereoLatencyTimer("GRAPHICS_ClearStereoLatency");
TIMER_Interval   GRAPHICS_ClearMonoLatencyTimer("GRAPHICS_ClearMonoLatency");
TIMER_Frequency  GRAPHICS_DisplayFrequencyTimer("GRAPHICS_DisplayFrequency");
TIMER_Interval   GRAPHICS_DisplayLatencyTimer("GRAPHICS_DisplayLatency");
TIMER_Interval   GRAPHICS_TextLatencyTimer("GRAPHICS_TextLatency");
TIMER_Interval   GRAPHICS_DrawLatencyTimer("GRAPHICS_DrawLatency");
TIMER_Interval   GRAPHICS_SwapBufferLatencyTimer("GRAPHICS_SwapBufferLatency");
TIMER            GRAPHICS_SwapBufferAbsoluteTimer("GRAPHICS_SwapBufferAbsoluteTime");
TIMER_Frequency  GRAPHICS_GlutKeyboardFrequencyTimer("GRAPHICS_GlutKeyboardFrequency");
TIMER_Frequency  GRAPHICS_GlutDisplayFrequencyTimer("GRAPHICS_GlutDisplayFrequency");
TIMER_Frequency  GRAPHICS_GlutIdleFrequencyTimer("GRAPHICS_GlutIdleFrequency");

double GRAPHICS_ClearStereoLatency;
double GRAPHICS_ClearMonoLatency;
double GRAPHICS_DisplayLatency;
double GRAPHICS_DrawLatency;
double GRAPHICS_DisplayFrequencyPeriod;
double GRAPHICS_SwapBufferLatency;
double GRAPHICS_SwapBufferAbsoluteTime;
float   GRAPHICS_CalibCentre[GRAPHICS_3D];

/******************************************************************************/

void (*GRAPHICS_KeyboardFunction)( BYTE key, int x, int y )=NULL;
void (*GRAPHICS_DrawFunction)( void )=NULL;
void (*GRAPHICS_IdleFunction)( void )=NULL;

/******************************************************************************/

struct STR_TextItem GRAPHICS_ColorText[] = 
{
    { RED       ,"RED"        },
    { YELLOW    ,"YELLOW"     },
    { BLUE      ,"BLUE"       },
    { WHITE     ,"WHITE"      },
    { GREEN     ,"GREEN"      },
    { TURQUOISE ,"TURQUOISE"  },
    { PURPLE    ,"PURPLE"     },
    { BLACK     ,"BLACK"      },
    { LIGHTBLUE ,"LIGHTBLUE"  },
    { DARKBLUE  ,"DARKBLUE"   },
    { GREY      ,"GREY"       },
    { DARKGREY  ,"DARKGREY"   },
    { LIGHTBLACK,"LIGHTBLACK" },
    { LIGHTGREY ,"LIGHTGREY"  },
    { STR_TEXT_ENDOFTABLE     }
};

/******************************************************************************/
/******************************************************************************/

BOOL GRAPHICS_ColorCode( int &code, char *text )
{
BOOL ok=FALSE;

    ok = STR_TextCode(GRAPHICS_ColorText,code,text);

    return(ok);
}

/******************************************************************************/

int GRAPHICS_ColorCode( char *text )
{
BOOL ok;
int code=WHITE;

    ok = GRAPHICS_ColorCode(code,text);

    return(code);
}

/******************************************************************************/

BOOL GRAPHICS_Cnfg( char *file )
{
BOOL ok;

    CONFIG_reset();

    // Set up variable table for configuration...
    CONFIG_set("Description",GRAPHICS_Description);
    CONFIG_set("StereoMode",GRAPHICS_StereoModeString);
    CONFIG_set("DefaultMode",GRAPHICS_DefaultModeString);
    CONFIG_set("ClearColor",GRAPHICS_ClearColorString);
    CONFIG_set("Pixels",GRAPHICS_DisplayPixels,GRAPHICS_2D);
    CONFIG_set("Frequency",GRAPHICS_DisplayFrequency);
    CONFIG_set("Rotation",GRAPHICS_DisplayRotation,GRAPHICS_3D);
    CONFIG_set("Xmin,Xmax",GRAPHICS_DisplaySize[GRAPHICS_X],GRAPHICS_RANGE);
    CONFIG_set("Ymin,Ymax",GRAPHICS_DisplaySize[GRAPHICS_Y],GRAPHICS_RANGE);
    CONFIG_set("Znear,Zfar",GRAPHICS_DisplaySize[GRAPHICS_Z],GRAPHICS_RANGE);
    CONFIG_set("FocalPlane",GRAPHICS_FocalPlane);
    CONFIG_set("PupilToCentre",GRAPHICS_PupilToCentre);
    CONFIG_set("EyeCentre",GRAPHICS_EyeCentre,GRAPHICS_3D);
    CONFIG_set("LightPosition",GRAPHICS_LightPosition,GRAPHICS_3D);
    CONFIG_set("LightColor",GRAPHICS_LightColor,GRAPHICS_RGB);
    CONFIG_set("Calibration",GRAPHICS_CalibrationFile);
    CONFIG_setBOOL("SpaceBall",GRAPHICS_SpaceBall);
    CONFIG_setBOOL("StereoDepth",GRAPHICS_StereoDepth);
    CONFIG_set("DisplayDelay",GRAPHICS_DisplayDelayMinimum);

    // Load configuration file...
    
    if( STR_null(file) )
    {
        file = FILE_Calibration(GRAPHICS_Config);
    }
    //printf("%s stringus \n", file);
    printf("file=[%s]\n",file);
    
    if( !CONFIG_read(file) )
    {   
        //printf(" o no O NO\n");
        printf("CONFIG_read(%s) Failed.\n",file);
        return(FALSE);
    }

    if( GRAPHICS_DisplaySize[GRAPHICS_Z][GRAPHICS_MIN] == 0.0 )
    {
        GRAPHICS_DisplaySize[GRAPHICS_Z][GRAPHICS_MIN] = 1.0;
    }

    if( GRAPHICS_DisplaySize[GRAPHICS_Z][GRAPHICS_MAX] == 0.0 )
    {
        GRAPHICS_DisplaySize[GRAPHICS_Z][GRAPHICS_MAX] = 1000.0;
    }

    // Process display  mode...
    if( CONFIG_readflag("DefaultMode") )
    {
        if( !GRAPHICS_DisplayModeParse(GRAPHICS_DefaultMode,GRAPHICS_DefaultModeString) )
        {
            printf("this dispmodeparse failed");
            return(FALSE);
        }
    }

    // Process stereo mode...
    if( CONFIG_readflag("StereoMode") )
    {
        if( !GRAPHICS_StereoModeParse(GRAPHICS_StereoMode,GRAPHICS_StereoModeString) )
        {
            printf("this stereomodeparse failed");
            return(FALSE);
        }
    }

    if( GRAPHICS_StereoMode == GRAPHICS_STEREO_OCULUS )
    {
        GRAPHICS_DefaultMode = GRAPHICS_DISPLAY_STEREO;
    }

    // Process clear color...
    if( CONFIG_readflag("ClearColor") )
    {
        if( !GRAPHICS_ColorCode(GRAPHICS_ClearColorDefault,GRAPHICS_ClearColorString) )
        {
            printf("this clear color thing failed");
            return(FALSE);
        }
    }

    // Default light position if not specified...
    if( !CONFIG_readflag("LightPosition") )
    {
        GRAPHICS_LightPosition[0] = GRAPHICS_CalibCentre[0];
        GRAPHICS_LightPosition[1] = GRAPHICS_CalibCentre[1];
        GRAPHICS_LightPosition[2] = GRAPHICS_EyeCentre[2];
    }

    CONFIG_list(GRAPHICS_debugf);

    // Convert eye-centre to a matrix (more useful)...
    SPMX_xyz2pomx(GRAPHICS_EyeCentre,GRAPHICS_EyeCentrePOMX);
    //printf("returning true ...");
    return(TRUE);
}
