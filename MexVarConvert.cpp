/******************************************************************************/

BOOL MexVarToMatrix( const mxArray *mexvar, matrix &mvar, BOOL match )
{
int i,j,k,n;
const mwSize *d;
double *dptr;

    n = mxGetNumberOfDimensions(mexvar);
    d = mxGetDimensions(mexvar);
    dptr = mxGetPr(mexvar);
    
    if( n != 2 )
    {
        mexPrintf("n = %d\n",n);
        return(FALSE);
    }
    
    //mexPrintf("mvar=%dx%d, mexvar=%dx%d\n",mvar.rows(),mvar.cols(),d[0],d[1]);
    
    if( match && ((mvar.rows() != d[0]) || (mvar.cols() != d[1])) )
    {
        return(FALSE);
    }
    
    mvar.dim(d[0],d[1]);
    
    for( k=0,j=1; (j <= d[1]); j++ )
    {
        for( i=1; (i <= d[0]); i++ )
        {
            //mexPrintf("mvar(%d,%d) dptr[%d]=%.2lf\n",i,j,k,dptr[k]);
            mvar(i,j) = dptr[k++];
        }
    }
    
    return(TRUE);
}

/******************************************************************************/

BOOL MexVarToMatrix( const mxArray *mexvar, matrix &matvar )
{
BOOL match=FALSE;
BOOL ok;

    ok = MexVarToMatrix(mexvar,matvar,match);

    return(ok);
}
    
/******************************************************************************/
    
BOOL MexVarToDouble( const mxArray *mexvar, double &dvar )
{
    dvar = (*mxGetPr(mexvar));
    
    return(TRUE);
}

/******************************************************************************/

BOOL MexVarToInt( const mxArray *mexvar, int &ivar )
{
    ivar = (int)(*mxGetPr(mexvar));
    
    return(TRUE);
}

/******************************************************************************/