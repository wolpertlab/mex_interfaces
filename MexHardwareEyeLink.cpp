/******************************************************************************/

CRITICAL_SECTION EyeLinkLoopCriticalSection;

BOOL    EyeLinkStartFlag=FALSE;
HANDLE  EyeLinkLoopHandle=NULL;
BOOL    EyeLinkLoopExit=FALSE;
BOOL    EyeLinkLoopRunning=FALSE;
DWORD   WINAPI EyeLinkLoop( LPVOID parameters );

TIMER_Frequency EyeLinkLoopFrequency("EyeLinkLoop");
TIMER_Interval  EyeLinkLoopLatency("EyeLinkLoop");
TIMER_Frequency EyeLinkFrameFrequency("EyeLinkFrame");

TIMER   EyeLinkFrameTimer("EyeLinkFrameTimer");

int     EyeLinkFrameCount=0;
double  EyeLinkFrameTimeStamp;

int     EyeLinkLatestFrame=0;
double  EyeLinkLatestTimeStamp=0.0;
double  EyeLinkLatestEyeXY[2];
double  EyeLinkLatestPupilSize=0.0;

matrix  EyeLinkCalibrationMatrix(2,3);
matrix  EyeLinkDataRaw(3,1);
matrix  EyeLinkDataCalibrated(2,1);

/******************************************************************************/

DWORD   WINAPI EyeLinkLoop( LPVOID parameters )
{
BOOL Exit,ok,ready;
double TimeStamp,EyeXY[2],PupilSize;

    // Set running flag to TRUE.
    EyeLinkLoopRunning = TRUE;

    Exit = FALSE;

    while( !Exit )
    {
        EyeLinkLoopFrequency.Loop();

        EyeLinkLoopLatency.Before();

        ok = EYELINK_FrameNext(TimeStamp,EyeXY,PupilSize,ready);

        if( ok && ready )
        {
            EyeLinkFrameFrequency.Loop();
            EyeLinkFrameCount++;
            EyeLinkFrameTimeStamp = EyeLinkFrameTimer.ElapsedSeconds();

            EyeLinkDataRaw(1,1) = EyeXY[0];
            EyeLinkDataRaw(2,1) = EyeXY[1];
            EyeLinkDataRaw(3,1) = 1.0;

            EyeLinkDataCalibrated = EyeLinkCalibrationMatrix * EyeLinkDataRaw;

            EnterCriticalSection(&EyeLinkLoopCriticalSection);

            EyeLinkLatestFrame = EyeLinkFrameCount;
            EyeLinkLatestTimeStamp = EyeLinkFrameTimeStamp;

            EyeLinkLatestEyeXY[0] = EyeLinkDataCalibrated(1,1);
            EyeLinkLatestEyeXY[1] = EyeLinkDataCalibrated(2,1);

            EyeLinkLatestPupilSize = PupilSize;

            LeaveCriticalSection(&EyeLinkLoopCriticalSection);
        }

        // Exit flag is passed as a pointer in the parameters.
        Exit = *((BOOL *)parameters);

        EyeLinkLoopLatency.After();

        Sleep(0);
    }

    //mexPrintf("EyeLinkLoop shutting down...\n");

    // Set running flag to FALSE.
    EyeLinkLoopRunning = FALSE;

    return(0);
}

/******************************************************************************/

void EyeLinkTimerResults( void )
{
    // Print results of timers...
    EyeLinkLoopFrequency.Results(mexPrintf);
    EyeLinkLoopLatency.Results(mexPrintf);
    EyeLinkFrameFrequency.Results(mexPrintf);

    // Reset timers...
    EyeLinkLoopFrequency.Reset();
    EyeLinkLoopLatency.Reset();
    EyeLinkFrameFrequency.Reset();
}

/******************************************************************************/

BOOL EyeLinkStarted( void )
{
BOOL flag;

    flag = EyeLinkStartFlag;

    return(flag);
}

/******************************************************************************/

BOOL EyeLinkStart( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;

    if( EyeLinkStarted() )
    {
        return(TRUE);
    }

    EyeLinkFrameTimer.Reset();
    EyeLinkLoopExit = FALSE;

    if( !EYELINK_Open() )
    {
        mexPrintf("mexRobotFunc: Cannot open EyeLink.\n");
        ok = FALSE;
    }
    else
    if( !EYELINK_Start() )
    {
        mexPrintf("mexRobotFunc: Cannot start EyeLink.\n");
        ok = FALSE;
    }
    else
    if( (EyeLinkLoopHandle=CreateThread(NULL,0,EyeLinkLoop,(void *)&EyeLinkLoopExit,0,NULL)) == NULL )
    {
        mexPrintf("mexRobotFunc: Cannot start EyeLink loop.\n");
        ok = FALSE;
    }
    else
    {
        mexPrintf("mexRobotFunc: EyeLink started\n");
        EyeLinkStartFlag = TRUE;
    }

    return(ok);
}

/******************************************************************************/

BOOL EyeLinkStop( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE,flag=FALSE;

    if( EyeLinkStarted() )
    {
        mexPrintf("mexRobotFunc: EyeLink stoppping.\n");
        flag = TRUE;
    }

    // Stop EyeLink Loop thread if it's running.
    HardwareThreadStop(&EyeLinkLoopRunning,&EyeLinkLoopExit,"EyeLink Loop");

    EYELINK_Stop();
    EYELINK_Close();

    EyeLinkStartFlag = FALSE;

    if( flag )
    {
        EyeLinkTimerResults();
    }

    return(ok);
}

/******************************************************************************/

BOOL EyeLinkStopCritical( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok;

    EnterCriticalSection(&HardwareWatchDogCriticalSection);
    ok = EyeLinkStop(nlhs,plhs,nrhs,prhs);
    LeaveCriticalSection(&HardwareWatchDogCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL EyeLinkGetLatest( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int i,j;
double dtrash[2];
double *FN,*TS,*EP,*PS;

    if( !EyeLinkStarted() )
    {
        mexPrintf("mexRobotFunc: EyeLink not started.\n");
        return(FALSE);
    }

    // Point to trash variables in case not specified.
    FN = dtrash;
    TS = dtrash;
    EP = dtrash;
    PS = dtrash;

    j = 2;
    if( nlhs >= j ) // Frame number.
    {
        plhs[j-1] = mxCreateDoubleMatrix(1,1,mxREAL);
        FN = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Frame time-stamp.
    {
        plhs[j-1] = mxCreateDoubleMatrix(1,1,mxREAL);
        TS = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Eye XY position.
    {
        plhs[j-1] = mxCreateDoubleMatrix(2,1,mxREAL);
        EP = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Eye pupil size.
    {
        plhs[j-1] = mxCreateDoubleMatrix(1,1,mxREAL);
        PS = mxGetPr(plhs[j-1]);
    }

    EnterCriticalSection(&EyeLinkLoopCriticalSection);

    // Frame number, time-stamp.
    (*FN) = (double)EyeLinkLatestFrame;
    (*TS) = EyeLinkLatestTimeStamp;

    // Eye XY position.
    for( i=0; (i < 2); i++ )
    {
        EP[i] = EyeLinkLatestEyeXY[i];
    }

    // Eye pupil size.
    (*PS) = EyeLinkLatestPupilSize;

    LeaveCriticalSection(&EyeLinkLoopCriticalSection);

    return(ok);
}

/******************************************************************************/

BOOL EyeLinkSetCalibration( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int j=1;
matrix CalibrationMatrix(2,3);

    if( !EyeLinkStarted() )
    {
        mexPrintf("mexRobotFunc: EyeLink not started.\n");
        return(FALSE);
    }

    if( !MexVarToMatrix(prhs[j++],CalibrationMatrix,TRUE) )
    {
        mexPrintf("mexRobotFunc: Invalid dimensions for EyeLink calibration matrix.\n");
        return(ok);
    }

    EnterCriticalSection(&EyeLinkLoopCriticalSection);

    EyeLinkCalibrationMatrix = CalibrationMatrix;

    LeaveCriticalSection(&EyeLinkLoopCriticalSection);

    return(ok);
}

/******************************************************************************/
