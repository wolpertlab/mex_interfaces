function MakeMex(cpp_file, architecture)
%MAKEMEX compiles mex function for a given cpp file and a 32/64bit arch
%    MAKEMEX(cpp_file, architecture) compiles the mex function on file name
%    cpp_file and on architecture number architecture.
    
    % parse inputs
    pdi_lib = sprintf(' -lPDI_%d.lib ', architecture);
    numrep_lib = sprintf(' -lNR%d.lib ', architecture);
    senso_lib = sprintf(' -lS826_%d.lib ', architecture);
    eyelink_lib = sprintf(' -leyelink_core%d.lib ', architecture);
    motor_lib = sprintf(' -lMOTOR%d.lib ', architecture);
    mex_lib = sprintf(' -Lu:\\lib\\%dbit_mex ', architecture);
    include = sprintf(' -Iu:\\include\\%dbit ', architecture);
    %build command
    command_string = strcat('mex LINKFLAGS="$LINKFLAGS /NODEFAULTLIB:LIBC.lib /NODEFAULTLIB:MSVCRT.lib"',...
                            sprintf(' %s',cpp_file),...
                            ' -lLIBCMT.lib -lwinmm.lib  ',...
                            eyelink_lib, senso_lib, motor_lib, numrep_lib, ...
                            pdi_lib, mex_lib, include);
    %evaluate command (build mex)
    eval(command_string);
    clear mex

end
