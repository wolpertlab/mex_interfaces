ok = mexHardwareFunc(0,'ROBOT_Stiff2BOT-Right');

Exit = false;

CollectTime = 10.0;
tic;
while( ok && ~Exit )
    [ ok,RS,RP,RV,RF ] = mexHardwareFunc(2);
    fprintf('H=%d,Rx=%.1f,Ry=%.1f\n',RS(1),RP(1),RP(2));
    
    CurrentTime = toc;
    Exit = CurrentTime >= CollectTime;
    pause(0.1);
end

fprintf('About to stop...\n');
pause(0.1);

ok = mexHardwareFunc(1);
