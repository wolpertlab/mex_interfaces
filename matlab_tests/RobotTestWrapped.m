%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;
%clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path for 'WL' functions, etc.
%addpath(genpath('U:\wolpert\WL'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%RobotLeft = 'ROBOT_vBOT-Left';
%RobotRight = 'ROBOT_vBOT-Right';
%RobotLeft = 'ROBOT_Stiff2BOT-Right(QF)';
RobotLeft = 'ROBOT_vBOT-Right';

Robot = wl_robot(RobotLeft);
%Robot = wl_robot(RobotLeft,RobotRight);
Hardware = wl_hardware(Robot);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%ViscousMatrix = [ 0 -0.1 0; 0.1 0 0; 0 0 0 ];
ViscousMatrix = [ -0.1 0 0; 0 -0.1 0; 0 0 0 ];

SpringHome = [ 0 0 0 ]';
SpringConstant = -10.0;
DampingConstant = 0.0;

ChannelStart = [ 0 0 0 ]';
ChannelTarget = [ 0 12 0 ]';

PMoveTarget = [ 0 12 0 ]';
PMoveMoveTime = 0.8;
PMoveHoldTime = 2.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ok = Robot.Start();
if( ~ok )
    error('Robot start failed');
end

%RobotField = false;
RobotField = true;

loop_period = 0.1;
loop_count = 0;
loop_timeout = 20.0;
loop_timer = tic;
loop_time = 0.0;

ok = Hardware.DataStart();

while( ok && (toc(loop_timer) <= loop_timeout) )
    loop_count = loop_count + 1;
    loop_time = loop_time + loop_period;
    
    if( RobotField )
        if( (loop_count*loop_period) == 5.0 ) % 5 seconds
            fprintf(1,'Force-field on...\n');
            ok = Robot.FieldViscous(ViscousMatrix);
            %ok = Robot.FieldSpring(SpringHome,SpringConstant,DampingConstant);
            %ok = Robot.FieldChannel(ChannelTarget,SpringConstant,DampingConstant);
            %ok = Robot.FieldPMove(PMoveTarget,PMoveMoveTime,PMoveHoldTime);

            fprintf(1,'Field ok=%d\n',ok);
            ok = Robot.RampUp();
        end

        if( (loop_count*loop_period) == 10.0 ) % 10 seconds
            fprintf(1,'Paused...\n');
            pause(2.0);
        end
        
        if( (loop_count*loop_period) == 15.0 ) % 15 seconds
            fprintf(1,'Force-field off...\n');
            ok = Robot.RampDown();
        end
    end
    
    ok = Robot.GetLatest();
    if( Robot.RobotCount == 2 )
        fprintf(1,'ok=%d, Active=%d,%d, Field=%d(%.1f),%d(%.1f) Pxy[0]=%.1f,%.1f Pxy[1]=%.1f,%.1f\n',ok,Robot.Active(1),Robot.Active(2),Robot.FieldType(1),Robot.FieldRampValue(1),Robot.FieldType(2),Robot.FieldRampValue(2),Robot.Position(1,1),Robot.Position(2,1),Robot.Position(1,2),Robot.Position(2,2));
    else
        fprintf(1,'ok=%d, Active=%d, Field=%d(%.1f), Pxy=%.1f,%.1f\n',ok,Robot.Active,Robot.FieldType,Robot.FieldRampValue,Robot.Position(1,1),Robot.Position(2,1));
    end
    
    pause(loop_period);
end

if( ~ok )
    error('Test loop has exit with ok=FALSE');
end

ok = Hardware.DataStop();
[ ok,FrameData ] = Hardware.DataGet();
FrameData = FrameData{1}

ok = Robot.Stop();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(1);
clf;

pm = 2;
pn = 2;
pp = 0;

pp = pp + 1;
subplot(pm,pn,pp);
x = FrameData.TimeStamp; % Time
y = FrameData.RobotFieldRamp; % Field Ramp
plot(x,y,'k-');
xlabel('Time (sec)');
ylabel('Field Ramp');
axis([ (min(x)-0.1) (max(x)+0.1) -0.1 1.1 ]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
