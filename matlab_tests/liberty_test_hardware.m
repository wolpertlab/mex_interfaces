function dat = liberty_test(cfg, wlhardware)
    if nargin < 1
        cfg = 'LIBERTY.CFG';
    end
    if nargin < 2
        wlhardware = false;
    end
    
    liberty_obj = wl_liberty(cfg);
    ok =liberty_obj.Start;
    
    
    KbQueueCreate();
    KbQueueStart();

    out = [];
    
    if wlhardware
        hardware_obj = wl_hardware(liberty_obj);
        hardware_obj.DataStart;
        dat.obj = hardware_obj;
    end
    Exit=0;
    while ~Exit && ok
        if ~wlhardware
            liberty_obj.GetLatest;

            ds.FrameCount = liberty_obj.FrameCount;
            ds.FrameTime = liberty_obj.FrameTime;
            ds.SensorPosition = liberty_obj.SensorPosition;
            ds.SensorOrientation = liberty_obj.SensorOrientation;
            ds.SensorDistortion = liberty_obj.SensorDistortion;  


            out = [out ds];
        else
            ok = hardware_obj.GetLatest();
        end
        [pressed, firstPress, ~, ~, ~] = KbQueueCheck();
        if (pressed && firstPress(KbName('ESC')))
            break;
        end
        pause(0.01);
    end

    if ~wlhardware
        liberty_obj.Stop;
    else
        hardware_obj.DataStop;
        [ ok,raw,names ] = hardware_obj.DataGet;
        liberty_obj.Stop;
    end
    
    PlotColor = { 'r' 'g' 'b' };
    sx = 3;
    sy = 1;
    sz = 1;
    if ~wlhardware
        [~, SensorCount] = size(out(1).SensorOrientation);
        for i=1:SensorCount
            SensorPosition.(char("s" +i)) = [];
            SensorAngles.(char("s" +i)) = [];
            t.(char("s" +i)) = [];
            for j=1:length(out)
                SensorPosition.(char("s" +i)) = [SensorPosition.(char("s" +i)) ...
                                                 out(j).SensorPosition(:,i)];
                SensorAngles.(char("s" +i)) = [SensorAngles.(char("s" +i)) ...
                                               out(j).SensorOrientation(:,i)];
                t.(char("s" +i)) = [t.(char("s" +i)) out(j).FrameTime];
            end
        end
      
        for i=1:SensorCount
            figure(i);
            for j=1:3
                subplot(sx,sy,sz);
                sz = sz + 1;
                plot(t.(char("s" +i)), ...
                     SensorPosition.(char("s" +i))(j,:), ...
                     sprintf('%s-',PlotColor{j}) );
            end
            sz = 1;
            subplot(sx,sy,sz);
            axis([ (min(t.(char("s" +i)))-0.1) (max(t.(char("s" +i)))+0.1) -50 50 ]);
            xlabel('Time (sec)');
            ylabel('Position (cm)');
            title(sprintf('Sensor %d',i));
            
        end
        dat.sp = SensorPosition;
        dat.t = t;
        dat.out = out;
    else
        dat.raw = raw;
    end
    
   
    
end
