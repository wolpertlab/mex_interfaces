%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path for 'WL' functions, etc.
addpath(genpath('U:\wolpert\WL'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Robot = wl_robot('ROBOT_vBOT-Left','ROBOT_vBOT-Right');
Hardware = wl_hardware(Robot);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ViscousMatrix = [ 0 -0.2 0; 0.2 0 0; 0 0 0 ];

SpringHome = [ 0 0 0 ]';
SpringConstant = -10.0;
DampingConstant = 0.0;

ChannelStart = [ 0 0 0 ]';
ChannelTarget = [ 0 12 0 ]';

PMoveTarget = [ 0 12 0 ]';
PMoveTime = 0.5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ok = Robot.Start();
if( ~ok )
    error('Robot start failed');
end

RobotLeft = 0;
RobotRight = 1;

%RobotField = false;
RobotField = true;

loop_period = 0.25;
loop_count = 0;
loop_timeout = 20.0;
loop_timer = tic;
loop_time = 0.0;

ok = Hardware.DataStart();

while( ok && (toc(loop_timer) <= loop_timeout) )
    loop_count = loop_count + 1;
    loop_time = loop_time + loop_period;
    
    if( RobotField )
        if( (loop_count*loop_period) == 5.0 ) % 5 seconds
            fprintf(1,'Force-field on...\n');
            
            SpringHome = [ -10 0 0 ]'; % Left robot.
            PMoveTarget = SpringHome;
            ChannelTarget = [ -10 12 0 ]';
            %ok = Robot.FieldViscous(ViscousMatrix,RobotLeft);
            %ok = Robot.FieldSpring(SpringHome,SpringConstant,DampingConstant,RobotLeft);
            %ok = Robot.FieldPMove(PMoveTarget,PMoveTime,RobotLeft);
            %ok = Robot.FieldChannel(ChannelTarget,SpringConstant,DampingConstant,RobotLeft);
            %fprintf(1,'Field ok=%d\n',ok);
            
            SpringHome = [ 10 0 0 ]'; % Right robot.
            PMoveTarget = SpringHome;
            ChannelTarget = [ 10 12 0 ]';
            %ok = Robot.FieldViscous(ViscousMatrix,RobotRight);
            %ok = Robot.FieldSpring(SpringHome,SpringConstant,DampingConstant,RobotRight);
            %ok = Robot.FieldPMove(PMoveTarget,PMoveTime,RobotRight);
            %ok = Robot.FieldChannel(ChannelTarget,SpringConstant,DampingConstant,RobotRight);
            %fprintf(1,'Field ok=%d\n',ok);

            BimanualSpringType = 0; % Free to move in all axes.
            %BimanualSpringType = 1; % X-clamped, others free.
            %BimanualSpringType = 2; % Y-clamped, others free.
            ok = Robot.FieldBimanualSpring(BimanualSpringType,SpringConstant,DampingConstant);
            fprintf(1,'Field ok=%d\n',ok);
            
            ok = Robot.RampUp();
        end

        if( (loop_count*loop_period) == 15.0 ) % 15 seconds
            fprintf(1,'Force-field off...\n');
            ok = Robot.RampDown();
        end
    end
    
    ok = Robot.GetLatest();
    if( Robot.RobotCount == 2 )
        fprintf(1,'ok=%d, Active=%d,%d, Field=%d,%d, Position R[0]=%.1f,%.1f R[1]=%.1f,%.1f\n',ok,Robot.Active(1),Robot.Active(2),Robot.FieldType(1),Robot.FieldType(2),Robot.Position(1,1),Robot.Position(2,1),Robot.Position(1,2),Robot.Position(2,2));
    else
        fprintf(1,'ok=%d, Active=%d, Field=%d, Position=%.1f,%.1f\n',ok,Robot.Active,Robot.FieldType,Robot.Position(1,1),Robot.Position(2,1));
    end
    
    pause(loop_period);
end

if( ~ok )
    error('Test loop has exit with ok=FALSE');
end

ok = Hardware.DataStop();
[ ok,FrameData ] = Hardware.DataGet();
FrameData = FrameData{1}

%pause(0.2);

ok = Robot.Stop();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RobotColor = { 'r' 'b' };
AxisColor = { 'r' 'g' 'b' };

figure(1);
clf;

pm = 3;
pn = 3;
pp = 0;

pp = pp + 1;
subplot(pm,pn,pp);
hold on;

for robot=1:Robot.RobotCount
    x = FrameData.TimeStamp; 
    y = FrameData.RobotRamp(robot,:); 
    plot(x,y,sprintf('%s-',RobotColor{robot}));
end

xlabel('Time (sec)');
ylabel('Robot Ramp');
axis([ (min(x)-0.1) (max(x)+0.1) -0.1 1.1 ]);

pp = pp + 1;
subplot(pm,pn,pp);
hold on;

for robot=1:Robot.RobotCount
    x = FrameData.TimeStamp;
    y = FrameData.RobotFieldRamp(robot,:);
    plot(x,y,sprintf('%s-',RobotColor{robot}));
end

xlabel('Time (sec)');
ylabel('Field Ramp');
axis([ (min(x)-0.1) (max(x)+0.1) -0.1 1.1 ]);

pp = pp + 1;
subplot(pm,pn,pp);
hold on;

for robot=1:Robot.RobotCount
    x = FrameData.TimeStamp;
    y = FrameData.RobotFieldType(robot,:);
    plot(x,y,sprintf('%s-',RobotColor{robot}));
end

xlabel('Time (sec)');
ylabel('Field Type');
axis([ (min(x)-0.1) (max(x)+0.1) -0.5 10.5 ]);

for robot=1:Robot.RobotCount
    pp = pp + 1;
    subplot(pm,pn,pp);
    hold on;

    for i=1:2
        x = FrameData.TimeStamp; % Time
        y = squeeze(FrameData.RobotPosition(robot,i,:));
        plot(x,y,sprintf('%s-',AxisColor{i}));
    end
    
    xlabel('Time (sec)');
    ylabel('Position (cm)');
    title(sprintf('Robot[%d]',robot));
    axis([ (min(x)-0.1) (max(x)+0.1) -30.5 30.5 ]);
end

pp = pp + 1;

for robot=1:Robot.RobotCount
    pp = pp + 1;
    subplot(pm,pn,pp);
    hold on;

    for i=1:2
        x = FrameData.TimeStamp; % Time
        y = squeeze(FrameData.RobotForces(robot,i,:));
        plot(x,y,sprintf('%s-',AxisColor{i}));
    end
    
    xlabel('Time (sec)');
    ylabel('Forces (N)');
    title(sprintf('Robot[%d]',robot));
    axis([ (min(x)-0.1) (max(x)+0.1) -40.5 40.5 ]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
