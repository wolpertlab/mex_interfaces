/******************************************************************************/

CRITICAL_SECTION LibertyLoopCriticalSection;

BOOL    LibertyStartFlag=FALSE;
HANDLE  LibertyLoopHandle=NULL;
BOOL    LibertyLoopExit=FALSE;
BOOL    LibertyLoopRunning=FALSE;
DWORD   WINAPI LibertyLoop( LPVOID parameters );

STRING  LibertyConfig="";
int     LibertySensorCount=0;

matrix  LibertySensorPosition[LIBERTY_SENSOR_MAX],LibertySensorOrientation[LIBERTY_SENSOR_MAX];
int     LibertySensorDistortion[LIBERTY_SENSOR_MAX];

TIMER_Frequency LibertyLoopFrequency("LibertyLoop");
TIMER_Interval  LibertyLoopLatency("LibertyLoop");
TIMER_Frequency LibertyFrameFrequency("LibertyFrame");

TIMER   LibertyFrameTimer("LibertyFrameTimer");

int     LibertyFrameCount=0;
double  LibertyFrameTimeStamp;            

int     LibertyLatestFrame=0;
double  LibertyLatestTimeStamp=0.0;
matrix  LibertyLatestPosition[LIBERTY_SENSOR_MAX],LibertyLatestOrientation[LIBERTY_SENSOR_MAX];
int     LibertyLatestDistortion[LIBERTY_SENSOR_MAX];

/******************************************************************************/

DWORD WINAPI LibertyLoop( LPVOID parameters )
{
BOOL Exit,ok=TRUE,ready=FALSE;
int i;

    // Set running flag to TRUE.    
    LibertyLoopRunning = TRUE;

    Exit = FALSE;
    
    while( !Exit )
    {
        LibertyLoopFrequency.Loop();
        
        LibertyLoopLatency.Before();
        
        ok = LIBERTY_GetFrame(ready,LibertySensorPosition,LibertySensorOrientation,LibertySensorDistortion);
        
        if( ok && ready )
        {
            LibertyFrameFrequency.Loop();
            LibertyFrameCount++;
            LibertyFrameTimeStamp = LibertyFrameTimer.ElapsedSeconds();
            
            EnterCriticalSection(&LibertyLoopCriticalSection);
            
            LibertyLatestFrame = LibertyFrameCount;
            LibertyLatestTimeStamp = LibertyFrameTimeStamp;
            
            for( i=0; (i < LibertySensorCount); i++ )
            {
                LibertyLatestPosition[i] = LibertySensorPosition[i];
                LibertyLatestOrientation[i] = LibertySensorOrientation[i];
                LibertyLatestDistortion[i] = LibertySensorDistortion[i];
            }
            
            LeaveCriticalSection(&LibertyLoopCriticalSection);
        }
            
        // Exit flag is passed as a pointer in the parameters.
        Exit = *((BOOL *)parameters);
        
        LibertyLoopLatency.After();
        
        Sleep(0);
    }
    
    //mexPrintf("LibertyLoop shutting down...\n");
    
    // Set running flag to FALSE.    
    LibertyLoopRunning = FALSE;
    
    return(0);
}

/******************************************************************************/

void LibertyTimerResults( void )
{
    // Print results of timers...
    LibertyLoopFrequency.Results(mexPrintf);
    LibertyLoopLatency.Results(mexPrintf);
    LibertyFrameFrequency.Results(mexPrintf);
    
    // Reset timers...
    LibertyLoopFrequency.Reset();
    LibertyLoopLatency.Reset();
    LibertyFrameFrequency.Reset();
}

/******************************************************************************/

BOOL LibertyStarted( void )
{
BOOL flag;

    flag = LibertyStartFlag;
    
    return(flag);
}

/******************************************************************************/

BOOL LibertyStart( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
char *sptr;
int j;

    if( LibertyStarted() )
    {
        return(TRUE);
    }

    LibertySensorCount = 0;
    LibertyLoopExit = FALSE;

    if( (sptr=mxArrayToString(prhs[1])) != NULL )
    {
        strncpy(LibertyConfig,sptr,STRLEN);
    }

    if( STR_null(LibertyConfig) )
    {
        return(FALSE);
    }

    if( !LIBERTY_Open(LibertyConfig) )
    {
        mexPrintf("mexRobotFunc: Cannot open Liberty (%s).\n",LibertyConfig);
        ok = FALSE;
    }
    else
    if( !LIBERTY_Start() )
    {
        mexPrintf("mexRobotFunc: Cannot start Liberty.\n");
        ok = FALSE;
    }
    
    if( ok )
    {
        LibertyFrameTimer.Reset();

        if( (LibertyLoopHandle=CreateThread(NULL,0,LibertyLoop,(void *)&LibertyLoopExit,0,NULL)) == NULL )
        {
            mexPrintf("mexRobotFunc: Cannot start Liberty loop.\n");
            ok = FALSE;
        }
        else
        {
            mexPrintf("mexRobotFunc: Liberty started\n");
            LibertyStartFlag = TRUE;
            LibertySensorCount = LIBERTY_Sensors();
        }
    }

    
    j = 2;
    if( nlhs >= j ) // Sensor count.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(1,1,mxREAL); 
        (*mxGetPr(plhs[j-1])) = LibertySensorCount;
    }
    
    return(ok);
}

/******************************************************************************/

BOOL LibertyStop( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE,flag=FALSE;

    if( LibertyStarted() )
    {
        mexPrintf("mexRobotFunc: Liberty stoppping.\n");
        flag = TRUE;
    }

    // Stop Liberty Loop thread if it's running.
    HardwareThreadStop(&LibertyLoopRunning,&LibertyLoopExit,"Liberty Loop");

    LIBERTY_Stop();
    LIBERTY_Close();

    LibertyStartFlag = FALSE;
    
    if( flag )
    {
        LibertyTimerResults();
    }
    
    return(ok);
}    

/******************************************************************************/

BOOL LibertyStopCritical( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok;

    EnterCriticalSection(&HardwareWatchDogCriticalSection);
    ok = LibertyStop(nlhs,plhs,nrhs,prhs);
    LeaveCriticalSection(&HardwareWatchDogCriticalSection);
    
    return(ok);
}

/******************************************************************************/

BOOL LibertyGetLatest( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
BOOL ok=TRUE;
int i,j,k,sensor;
double dtrash[LIBERTY_SENSOR_MAX*3];
double *FN,*TS,*SP,*SO,*SD;

    if( !LibertyStarted() )
    {
        mexPrintf("mexRobotFunc: Liberty not started.\n");
        return(FALSE);
    }

    // Point to trash variables in case not specified.
    FN = dtrash;
    TS = dtrash;
    SP = dtrash;
    SO = dtrash;
    SD = dtrash;

    j = 2;
    if( nlhs >= j ) // Frame number.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(1,1,mxREAL); 
        FN = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Frame time-stamp.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(1,1,mxREAL); 
        TS = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Sensor position.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(3,LibertySensorCount,mxREAL); 
        SP = mxGetPr(plhs[j-1]);
    }

    j++;
    if( nlhs >= j ) // Sensor orientation.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(3,LibertySensorCount,mxREAL); 
        SO = mxGetPr(plhs[j-1]);
    }
    
    j++;
    if( nlhs >= j ) // Sensor distortion level.
    {               
        plhs[j-1] = mxCreateDoubleMatrix(1,LibertySensorCount,mxREAL); 
        SD = mxGetPr(plhs[j-1]);
    }

    EnterCriticalSection(&LibertyLoopCriticalSection);

    // Frame number, time-stamp.
    (*FN) = (double)LibertyLatestFrame;
    (*TS) = LibertyLatestTimeStamp;

    // Sensor position.
    for( k=0,sensor=0; (sensor < LibertySensorCount); sensor++ )
    {
        for( i=1; (i <= 3); i++ )
        {
            SP[k++] = LibertyLatestPosition[sensor](i,1);
        }
    }
        
    // Sensor orientation.
    for( k=0,sensor=0; (sensor < LibertySensorCount); sensor++ )
    {
        for( i=1; (i <= 3); i++ )
        {
            SO[k++] = LibertyLatestOrientation[sensor](i,1);
        }
    }
    
    // Sensor distortion level.
    for( k=0,sensor=0; (sensor < LibertySensorCount); sensor++ )
    {
        SD[k++] = (double)LibertyLatestDistortion[sensor];
    }

    LeaveCriticalSection(&LibertyLoopCriticalSection);
    
    return(ok);
}

/******************************************************************************/

